INSERT INTO `permisos` (`codigo`, `nombre`, `descripcion`) VALUES
(1, 'admin', 'Administrador, puede crear, editar y usuarios y acceder a todas las secciones del sistema.'),
(2, 'usuario_global', 'Usuario que puede acceder a todas las secciones del sistema, excepto a las de administración de usuarios y permisos.');