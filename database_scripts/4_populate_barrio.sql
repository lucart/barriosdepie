INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('1 de mayo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('16 de noviembre', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('2 de septiembre', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('20 de junio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('23 de abril', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('4 de febrero', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Achaval Peña', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Acosta', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Aeronáutico', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alberdi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alberto', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alborada Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alborada Sur', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alejandro Centeno', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Almirante Brown', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alta Córdoba', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Altamira', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alto Alberdi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alto Hermoso', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alto Palermo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Alto Verde', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Altos de Vélez Sársfield', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Altos de Villa Cabrera', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Altos San Martín', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Altos Sud de San Vicente', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ameghino Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ameghino Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación 1 De Mayo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Altamira', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Benjamin Matienzo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Cerveceros', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Empalme', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Farina', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Jardín Espinosa', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Kennedy', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Las Palmas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Los Plátanos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Palmar', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Panamericano', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Poeta Lugones', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Pueyrredon', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Residencial America', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Rosedal', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación San Fernando', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación San Pablo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Urca', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Vélez Sarsfield', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ampliación Yapeyu', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ana Maria Zumaran', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Apeadero La Tablada', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Arcos I', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Argüello', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Argüello Lourdes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Argüello Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Arturo Capdevila', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ate', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Autódromo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Avenida', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ayacucho', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Bajada De Piedra', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Bajada San Roque', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Bajo Galan', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Bajo General Paz', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Bella Vista', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Bella Vista Oeste', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Betania', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Bialet Masse', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Boedo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Brigadier San Martín', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cabaña del Pilar', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cabo Farina', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Caceres', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('California', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Camino A Villa Posse', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Carbo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Carola Lorenzini', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Carrara', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Caseros', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Centro', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Centro América', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cerro Chico', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cerro de las Rosas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cerro Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cerveceros', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Chateau Carreras', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ciudadela', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cofico', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Colinas de Bella Vista', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Colinas de Vélez Sársfield', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Colinas del Cerro', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Colinas del Cerro Ampliación', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Colón', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Colonia Lola', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Comandante Espora', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Comercial', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Congreso', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Consorcio Esperanza', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cooperativa La Unidad', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Córdoba 4', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Córdoba 5', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Corral de Palos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Country Club', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Country Fortín del Pozo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Country Jockey Club', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Country Las Delicias', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Country Lomas de la Carolina', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Covico', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Crisol Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Crisol Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Cupani', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('De Los Bioquímicos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Dean Funes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ducasse', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ejército Argentino', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('El Cabildo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('El Cerrito', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('El Pueblito', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('El Quebracho', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('El Refugio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('El Trébol', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Emaus', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Empalme', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Empalme Casas De Obreros Y Empleados', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Escobar', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Estación Flores', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ferrer', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ferreyra', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ferroviario Mitre', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Finca La Dorotea', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Arenales', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Artigas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Belgrano', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Bustos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Mosconi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Paz', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Pueyrredon', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('General Savio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Granadero Pringles', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Granja De Funes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Guarnición Aérea Cba', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Guarnición Militar Cba', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Guayaquil', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Güemes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Guiñazu', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Guiñazu Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Hipólito Yrigoyen', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Hogar Propio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Horizonte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Inaudi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Independencia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Industrial Este', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Industrial Oeste', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ipona', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ipv Ituzaingo Anexo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Irupe', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ituzaingo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ituzaingo Anexo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Jardín', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Jardín Del Pilar', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Jardín Del Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Jardín Espinosa', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Jardín Hipodromo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Jeronimo Luis De Cabrera', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Jockey Club', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('José Hernández', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('José I Rucci', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('José Ignacio Díaz Sección 1', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('José Ignacio Diaz Sección 2', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('José Ignacio Diaz Sección 3', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('José Ignacio Diaz Sección 4', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('José Ignacio Diaz Sección 5', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Juan B Justo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Juan Xxiii', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Juniors', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Kairos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Kennedy', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('La Carolina', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('La Floresta', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('La France', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('La Fraternidad', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('La Hortensia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('La Salle', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('La Toma', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Lamadrid', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Dalias', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Delicias', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Flores', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Lilas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Magnolias', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Margaritas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Palmas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Palmas Anexo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Playas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Rosas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Las Violetas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Liceo General Paz', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Lomas De San Martín', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Lomas Del Suquia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Alamos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Angeles', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Boulevares', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Ceibos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Eucaliptus', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Filtros', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Gigantes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Granados', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Jacarandaes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Josefinos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Naranjos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Olmos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Olmos Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Paraísos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Pinos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Plátanos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Principios', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Robles', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Los Sauces', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Maipu Sección 1', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Maipu Sección 2', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Maldonado', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Marcelo T De Alvear', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Marcos Sastre', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Marechal', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Maria Lastenia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Mariano Balcarce', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Mariano Fragueiro', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Marques Anexo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Marques De Sobremonte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Maurizi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Mercantil', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Militar General Deheza', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Mirador', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Miralta', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Mirizzi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Mitre', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Muller', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Nicolas Avellaneda', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Nuestro Hogar I', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Nueva Córdoba', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Nueva Córdoba Anexa', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Nueva Italia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Obrero', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Observatorio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ombu', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Oña', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('OSN', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Padre Claret', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Palermo Bajo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Palmar', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Panamericano', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Alameda', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Atlantica', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Capital', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Capital Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Chacabuco', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Corema', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque De La Vega Tres', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Don Bosco', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Futura', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Jorge Newbery', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Latino', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Liceo Sección 1', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Liceo Sección 2', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Liceo Sección 3', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Los Molinos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Modelo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Montecristo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque República', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque San Carlos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque San Vicente', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Sarmiento', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Tablada', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Parque Vélez Sarsfield', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Paso De Los Andes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Patria', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Patricios', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Patricios Este', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Patricios Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Patricios Oeste', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Poeta Lugones', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Policarpio Cabral', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Policial', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Portal Del Jacaranda', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Posta De Vargas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Primera Junta', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Providencia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Puente Blanco', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Quebrada De Las Rosas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Quinta Santa Ana', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Quintas De Arguello', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Quintas De San Jorge', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Ramón J Carcano', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Recreo Del Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Remedios De Escalada', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Renacimiento', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Rene Favaloro', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial America', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial Aragon', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial Chateau', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial Olivos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial San Carlos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial San Jorge', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial San Roque', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial Santa Ana', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial Santa Rosa', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Residencial Vélez Sarsfield', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Rivadavia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Rivera Indarte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Rogelio Martínez', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Rosedal', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Rosedal Anexo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Sachi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Antonio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Cayetano', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Daniel', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Felipe', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Fernando', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Francisco', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Ignacio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Javier', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San José', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Lorenzo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Lorenzo Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Luis De Francia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Marcelo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Martín', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Martín Anexo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Martín Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Nicolas', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Pablo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Pedro Nolasco', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Rafael', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Ramón', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Salvador', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('San Vicente', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Santa Cecilia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Santa Clara De Asis', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Santa Isabel Sección 1', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Santa Isabel Sección 2', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Santa Isabel Sección 3', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Santa Rita', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Sargento Cabral', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Sarmiento', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('SEP', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('SMATA', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Suárez', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Tablada Park', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Talleres Este', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Talleres Oeste', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Talleres Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Tejas Del Sur', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Teniente Benjamin Matienzo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Teodoro Felds', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Tranviarios', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('UOCRA', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Urca', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Uritorco', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Urquiza', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Valle Del Cerro', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Vicor', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa 4 De Agosto', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa 9 De Julio', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Adela', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Alberdi', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Alicia Risler', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Allende Parque', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Argentina', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Aspacia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Avalos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Azalais', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Azalais Oeste', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Belgrano', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Bustos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Cabrera', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Centenario', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Claret', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Claudina', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Corina', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Cornu', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Coronel Olmedo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa El Libertador', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Esquiu', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Eucarística', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa General Urquiza', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Gran Parque', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Mafekin', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Marta', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Martínez', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Páez', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Quisquizacate', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Retiro', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Revol', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Revol Anexo', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Rivadavia', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Rivera Indarte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Saldan', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa San Carlos', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa San Isidro', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Serrana', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Siburu', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Silvano Funes', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Solferino', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Union', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Villa Warcalde', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Vivero Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Yapeyu', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Yofre H', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Yofre I', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Yofre Norte', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Yofre Sud', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Arguello', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Centro América', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Colon', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Empalme', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Libertador', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Monseñor Pablo Cabrera', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Pueyrredon', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Rancagua', 496);
INSERT INTO barriosdepiedb.BARRIO(NOMBRE, CODIGO_LOCALIDAD) VALUES('Zona Rural CPC Ruta 20', 496);
