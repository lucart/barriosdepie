SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `barriosdepiedb` ;
USE `barriosdepiedb` ;

-- -----------------------------------------------------
-- Table `barriosdepiedb`.`PROVINCIA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`PROVINCIA` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`LOCALIDAD`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`LOCALIDAD` (
  `codigo` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `codigo_provincia` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_localidad_provincia_idx` (`codigo_provincia` ASC),
  CONSTRAINT `fk_localidad_provincia`
    FOREIGN KEY (`codigo_provincia`)
    REFERENCES `barriosdepiedb`.`PROVINCIA` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`BARRIO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`BARRIO` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `codigo_localidad` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_barrio_localidad_idx` (`codigo_localidad` ASC),
  CONSTRAINT `fk_barrio_localidad`
    FOREIGN KEY (`codigo_localidad`)
    REFERENCES `barriosdepiedb`.`LOCALIDAD` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`DIRECCION`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`DIRECCION` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `calle` VARCHAR(45) NULL,
  `numero` INT NULL,
  `codigo_postal` VARCHAR(45) NULL,
  `codigo_barrio` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_direccion_barrio_idx` (`codigo_barrio` ASC),
  CONSTRAINT `fk_direccion_barrio`
    FOREIGN KEY (`codigo_barrio`)
    REFERENCES `barriosdepiedb`.`BARRIO` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`PERSONAS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`PERSONAS` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `apellido` VARCHAR(45) NULL,
  `nombre` VARCHAR(45) NULL,
  `dni` VARCHAR(45) NULL,
  `edad` INT NULL,
  `fecha_nacimiento` DATE NULL,
  `codigo_direccion` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_personas_direccion_idx` (`codigo_direccion` ASC),
  CONSTRAINT `fk_personas_direccion`
    FOREIGN KEY (`codigo_direccion`)
    REFERENCES `barriosdepiedb`.`DIRECCION` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`PERMISOS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`PERMISOS` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `descripcion` VARCHAR(500) NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`USUARIOS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`USUARIOS` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `apellido` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `codigo_permiso` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_usuario_permiso_idx` (`codigo_permiso` ASC),
  CONSTRAINT `fk_usuario_permiso`
    FOREIGN KEY (`codigo_permiso`)
    REFERENCES `barriosdepiedb`.`PERMISOS` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`BENEFICIO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`BENEFICIO` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`SOCIOS`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`SOCIOS` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nro_socio` INT NULL,
  `cuil` INT NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`NIVEL_EDUCACION`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`NIVEL_EDUCACION` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`VIVIENDA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`VIVIENDA` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL,
  `codigo_vivienda` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_vivienda_vivienda_idx` (`codigo_vivienda` ASC),
  CONSTRAINT `fk_vivienda_vivienda`
    FOREIGN KEY (`codigo_vivienda`)
    REFERENCES `barriosdepiedb`.`VIVIENDA` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`TRABAJO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`Trabajo` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL,
  `fecha_inicio` DATE NULL,
  `fecha_fin` DATE NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `barriosdepiedb`.`MAYOR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`MAYOR` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `cant_pers_vivienda` INT NULL,
  `cant_hijos_cargo` INT NULL,
  `trabajo_actual` INT NULL,
  `trabajo_anterior` INT NULL,
  `oficio` VARCHAR(45) NULL,
  `curso_oficio` VARCHAR(50),
  `telefono` VARCHAR(30) NULL,
  `email` VARCHAR(45) NULL,
  `facebook` VARCHAR(45) NULL,
  `codigo_persona` INT NULL,
  `codigo_socio` INT NULL,
  `codigo_nivel_estudio` INT NULL,
  `codigo_vivienda` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_mayor_personas_idx` (`codigo_persona` ASC),
  INDEX `fk_mayor_socio_idx` (`codigo_socio` ASC),
  INDEX `fk_mayor_nivel_estudio_idx` (`codigo_nivel_estudio` ASC),
  INDEX `fk_mayor_vivienda_idx` (`codigo_vivienda` ASC),
  INDEX `fk_trabajo_actual_trabajo_idx` (`trabajo_actual` ASC),
  INDEX `fk_trabajo_anterior_trabajo_idx` (`trabajo_anterior` ASC),
  CONSTRAINT `fk_mayor_personas`
    FOREIGN KEY (`codigo_persona`)
    REFERENCES `barriosdepiedb`.`PERSONAS` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mayor_socio`
    FOREIGN KEY (`codigo_socio`)
    REFERENCES `barriosdepiedb`.`SOCIOS` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mayor_nivel_educacion`
    FOREIGN KEY (`codigo_nivel_estudio`)
    REFERENCES `barriosdepiedb`.`NIVEL_EDUCACION` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mayor_vivienda`
    FOREIGN KEY (`codigo_vivienda`)
    REFERENCES `barriosdepiedb`.`VIVIENDA` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_trabajo_actual_trabajo`
    FOREIGN KEY (`trabajo_actual`)
    REFERENCES `barriosdepiedb`.`TRABAJO` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_trabajo_anterior_trabajo`
    FOREIGN KEY (`trabajo_anterior`)
    REFERENCES `barriosdepiedb`.`TRABAJO` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`COORDINADOR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`COORDINADOR` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `codigo_persona` INT NULL,
  `codigo_socio` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_coordinador_persona_idx` (`codigo_persona` ASC),
  INDEX `fk_coordinador_socios_idx` (`codigo_socio` ASC),
  CONSTRAINT `fk_coordinador_personas`
    FOREIGN KEY (`codigo_persona`)
    REFERENCES `barriosdepiedb`.`PERSONAS` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_coordinador_socios`
    FOREIGN KEY (`codigo_socio`)
    REFERENCES `barriosdepiedb`.`SOCIOS` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`COMEDOR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`COMEDOR` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `observacion` VARCHAR(255) NULL,
  `codigo_cordinador` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_comedor_coordinador_idx` (`codigo_cordinador` ASC),
  CONSTRAINT `fk_comedor_coordinador`
    FOREIGN KEY (`codigo_cordinador`)
    REFERENCES `barriosdepiedb`.`COORDINADOR` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barriosdepiedb`.`MENOR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`MENOR` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `codigo_persona` INT NULL,
  `codigo_comedor` INT NULL,
  `codigo_tutor` INT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_menor_persona_idx` (`codigo_persona` ASC),
  INDEX `fk_menor_mayor_idx` (`codigo_tutor` ASC),
  INDEX `fk_menor_comedor_idx` (`codigo_comedor` ASC),
  CONSTRAINT `fk_menor_persona`
    FOREIGN KEY (`codigo_persona`)
    REFERENCES `barriosdepiedb`.`PERSONAS` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_menor_mayor`
    FOREIGN KEY (`codigo_tutor`)
    REFERENCES `barriosdepiedb`.`MAYOR` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_menor_comedor`
    FOREIGN KEY (`codigo_comedor`)
    REFERENCES `barriosdepiedb`.`COMEDOR` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `barriosdepiedb`.`BENEFICIOSOCIO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barriosdepiedb`.`BENEFICIOSOCIO` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `codigo_socio` INT NULL,
  `codigo_beneficio` INT NULL,
  `observacion` VARCHAR(100) NULL,
  `otros_beneficios` VARCHAR(45) NULL,
  `fecha_inicio` DATE NULL,
  `fecha_fin` DATE NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_beneficiosocio_socio_idx` (`codigo_socio` ASC),
  INDEX `fk_beneficiosocio_beneficio_idx` (`codigo_beneficio` ASC),
  CONSTRAINT `fk_beneficiosocio_socio`
    FOREIGN KEY (`codigo_socio`)
    REFERENCES `barriosdepiedb`.`SOCIOS` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beneficiosocio_beneficio`
    FOREIGN KEY (`codigo_beneficio`)
    REFERENCES `barriosdepiedb`.`BENEFICIO` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
