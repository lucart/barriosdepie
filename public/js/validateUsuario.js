function validate(id) {
  	var campos = "";
  	
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
  	if(!$("#apellido").val())
  		campos = campos + "Apellido \n";
  	if(!$("#username").val())
  		campos = campos + "Username \n";
  	if(!$("#password").val())
  		campos = campos + "Password \n";
  	if(!$("#email").val())
  		campos = campos + "Email \n";
  	if($("#permiso").val() == 0)
  		campos = campos + "Permiso \n";
  	
  	if(campos.length === 0){
  		$('form#guardarUsuarioForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
  }