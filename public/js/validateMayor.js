function validate(id) {
  	var campos = "";
  	
  	if(!$("#name").val())
  		campos = campos + "Nombre \n";
  	if($("#surname").val() == 0)
  		campos = campos + "Apellido \n";
    if(!$("#dni").val())
      campos = campos + "Dni \n";
    if(!$("#birthdate").val())
      campos = campos + "Fecha de Nacimiento \n";
  	if($("#provincia").val() == 0)
  		campos = campos + "Provincia \n";
    if($("#localidad").val() == 0)
      campos = campos + "Localidad \n";
    if($("#barrio").val() == 0)
      campos = campos + "Barrio \n";
    if(!$("#postalCode").val())
      campos = campos + "Codigo Postal \n";
    if(!$("#street").val())
      campos = campos + "Calle \n";
    if(!$("#number").val())
      campos = campos + "Número \n";
    if(!$("#nroScio").val())
      campos = campos + "Número de Socio \n";
    if(!$("#cuil").val())
      campos = campos + "Cuil \n";
    if(!$("#benefit_6").val())
      campos = campos + "Beneficio \n";
    if(!$("#dateStart_6").val())
      campos = campos + "Fecha de inicio de Beneficio \n";
    if(!$("#dateEnd_6").val())
      campos = campos + "Fecha de fin de Beneficio \n";
    if(!$("#observation_6").val())
      campos = campos + "Observaciones \n";
    if(!$("#phone").val())
      campos = campos + "Teléfono \n";
    if(!$("#email").val())
      campos = campos + "Email \n";
    if(!$("#facebook").val())
      campos = campos + "Facebook \n";
    if(!$("#level_education").val())
      campos = campos + "Nivel Educación \n";
    if(!$("#ofice").val())
      campos = campos + "Oficio \n";
    if(!$("#cantchild").val())
      campos = campos + "Cantidad de Hijos \n";
    if(!$("#canthome").val())
      campos = campos + "Cantidad personas en Vivienda \n";
    if(!$("#living_place").val())
      campos = campos + "Vivienda \n";
    if(!$("#descTraAct").val())
      campos = campos + "Descripción Trabajo Actual \n";
    if(!$("#fechaIniAct").val())
      campos = campos + "Fecha de inicio Trabajo Actual \n";
  	
  	if(campos.length === 0){
  		$('form#guardarMayorForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Mayor",
        text: "¿Está seguro que desea eliminar este mayor?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../MayorController/eliminarComedor?idMayor=" + id;
        }
    });
}