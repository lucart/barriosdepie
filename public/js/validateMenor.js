function validate(id) {
  	var campos = "";
  	
    if($("#tutor").val() == 0)
      campos = campos + "Tutor \n";
    if($("#comedor").val() == 0)
      campos = campos + "Comedor \n";
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
    if(!$("#apellido").val())
      campos = campos + "Apellido \n";
    if(!$("#dni").val())
      campos = campos + "Dni \n";
    if(!$("#fechaNac").val())
      campos = campos + "Fecha de Nacimiento \n";
  	if($("#provincia").val() == 0)
  		campos = campos + "Provincia \n";
    if($("#localidad").val() == 0)
      campos = campos + "Localidad \n";
    if($("#barrio").val() == 0)
      campos = campos + "Barrio \n";
    if(!$("#codigoPostal").val())
      campos = campos + "Codigo Postal \n";
    if(!$("#calle").val())
      campos = campos + "Calle \n";
    if(!$("#numero").val())
      campos = campos + "Numero \n";
  	
  	if(campos.length === 0){
  		$('form#guardarMenorForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Menor",
        text: "¿Está seguro que desea eliminar este comedor?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../MenorController/eliminarMenor?idMenor=" + id;
        }
    });
}