function validate(id) {
  	var campos = "";
  	
  	if(!$("#name").val())
  		campos = campos + "Nombre \n";
  	if($("#surname").val() == 0)
  		campos = campos + "Apellido \n";
    if(!$("#dni").val())
      campos = campos + "Dni \n";
    if(!$("#birthdate").val())
      campos = campos + "Fecha de Nacimiento \n";
  	if($("#provincia").val() == 0)
  		campos = campos + "Provincia \n";
    if($("#localidad").val() == 0)
      campos = campos + "Localidad \n";
    if($("#barrio").val() == 0)
      campos = campos + "Barrio \n";
    if(!$("#postalCode").val())
      campos = campos + "Codigo Postal \n";
    if(!$("#street").val())
      campos = campos + "Calle \n";
    if(!$("#number").val())
      campos = campos + "Número \n";
    if(!$("#nroScio").val())
      campos = campos + "Número de Socio \n";
    if(!$("#cuil").val())
      campos = campos + "Cuil \n";
    if(!$("#benefit_6").val())
      campos = campos + "Beneficio \n";
    if(!$("#dateStart_6").val())
      campos = campos + "Fecha de inicio de Beneficio \n";
    if(!$("#dateEnd_6").val())
      campos = campos + "Fecha de fin de Beneficio \n";
    if(!$("#observation_6").val())
      campos = campos + "Observaciones \n";
  	
  	if(campos.length === 0){
  		$('form#guardarCoordinadorForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Coordinador",
        text: "¿Está seguro que desea eliminar este coordinador?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../CoordinadorController/eliminarCoordinador?idCoordinador=" + id;
        }
    });
}