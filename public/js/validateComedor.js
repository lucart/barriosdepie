function validate(id) {
  	var campos = "";
  	
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
  	if($("#coordinador").val() == 0)
  		campos = campos + "Coordinador \n";
  	
  	if(campos.length === 0){
  		$('form#guardarComedorForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Comedor",
        text: "¿Está seguro que desea eliminar este comedor?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../ComedorController/eliminarComedor?idComedor=" + id;
        }
    });
}