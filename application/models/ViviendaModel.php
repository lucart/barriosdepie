<?php

class ViviendaModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getViviendas() {
		$query = $this->db->query("select vi.codigo, vi.descripcion
									from VIVIENDA vi
									where vi.codigo_vivienda is null ");
		return $query->result();
	}
	
	public function getViviendasPropietario() {
		$query = $this->db->query("select vi.codigo, vi.descripcion
									from VIVIENDA vi
									where vi.codigo_vivienda = 3");
		return $query->result();
	}

	public function getVivienda($id) {
		$query = $this->db->query("select vi.codigo, vi.descripcion
									from VIVIENDA vi
									where vi.codigo = $id ");
		return $query->result();
	}
	
	
	
}