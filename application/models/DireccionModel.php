<?php

class DireccionModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getDirecciones() {
		$query = $this->db->query("select * from DIRECCION");
		return $query->result();
	}

	public function getDireccion($id) {
		$query = $this->db->query("select * from DIRECCION where codigo = $id");
		return $query->result();
	}
	
	public function insertDireccion($street, $number, $postalCode, $barrio) {
		$queryIns = $this->db->query("INSERT INTO `DIRECCION` ( `calle`, `numero`,`codigo_postal`,`codigo_barrio`) 
									VALUES ('$street','$number',$postalCode, $barrio);");								
		$query = $this->db->query("SELECT MAX(codigo) as idDireccion from DIRECCION;");
		
		return $query->result();
	}
	
	public function updateDireccion($idDireccion, $calle, $numero, $codigoPostal, $barrio) {
      $query = $this->db->query("update DIRECCION
                                  set calle = '$calle',
                                      numero = $numero,
                                      codigo_barrio = $barrio,
                                      codigo_postal = '$codigoPostal'
                                      where codigo = $idDireccion;");
      $query = $this->db->query("SELECT MAX(codigo) as idDireccion from DIRECCION;");
		
		return $query->result();
	}
}