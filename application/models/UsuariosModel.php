<?php

class UsuariosModel extends CI_Model {

    function __construct() {
      parent::__construct();
      $this->load->database();
    }

    function login($username, $password) {
      $this -> db -> select('codigo, username, password, nombre, apellido, email, codigo_permiso');
      $this -> db -> from('USUARIOS');
      $this -> db -> where('username', $username);
      $this -> db -> where('password', MD5($password));
      $this -> db -> limit(1);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1) {
        return $query->result();
      } else {
        return false;
      }
    }

    public function getUsuarios() {
      $query = $this->db->query("select u.codigo, u.username, u.password, u.nombre, u.apellido, u.email, p.nombre as permiso
                                  from USUARIOS u 
                                  inner join PERMISOS p on u.codigo_permiso = p.codigo");
      return $query->result();
    }

    public function getUsuario($id) {
      $query = $this->db->query("select codigo, nombre, apellido, email, username, password, codigo_permiso from USUARIOS where codigo = $id");
      return $query->result();
    }

    public function insertUsuario($nombre, $apellido, $email, $username, $password, $idPermiso) {
      $query = $this->db->query("INSERT INTO `USUARIOS` ( `nombre`, `apellido`, `email`, `username`, `password`, `codigo_permiso`) VALUES ('$nombre', '$apellido', '$email', '$username', MD5('$password'), $idPermiso);");
      return true;
    }

    public function deleteUsuario($id) {
      $query = $this->db->query("delete from USUARIOS where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }

    public function updateUsuario($nombre, $apellido, $email, $username, $password, $idPermiso, $idUsuario) {
      $query = $this->db->query("update USUARIOS
                                  set nombre = '$nombre',
                                      apellido = '$apellido',
                                      email = '$email',
                                      username = '$username',
                                      password = MD5('$password'),
                                      codigo_permiso = $idPermiso
                                      where codigo = $idUsuario;");
      return true;
    }

    public function updateUsuarioSinPassword($nombre, $apellido, $email, $username, $idPermiso, $idUsuario) {
      $query = $this->db->query("update USUARIOS
                                  set nombre = '$nombre',
                                      apellido = '$apellido',
                                      email = '$email',
                                      username = '$username',
                                      codigo_permiso = $idPermiso
                                      where codigo = $idUsuario;");
      return true;
    }

    public function getPasswordByUserId($id) {
      $query = $this->db->query("select password from USUARIOS where codigo = $id");
      return $query->result();
    }


}
?>