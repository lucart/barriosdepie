<?php

class PersonasModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getPersnas() {
		$query = $this->db->query("select * from PERSONAS");
		return $query->result();
	}

	public function getPersona($id) {
		$query = $this->db->query("select * from PERSONAS where codigo = $id");
		return $query->result();
	}
	
	public function insertPersona($name, $surname, $dni, $birthdate, $idDireccion) {
		$queryIns = $this->db->query("INSERT INTO `PERSONAS` ( `nombre`, `apellido`,`dni`,`fecha_nacimiento`,`codigo_direccion`) 
									VALUES ('$name','$surname',$dni, '$birthdate', $idDireccion);");								
		$query = $this->db->query("SELECT MAX(codigo) as idPersona from PERSONAS;");
		
		return $query->result();
	}
	
	public function updatePersona($idPersona, $nombre, $apellido, $dni, $fechaNac, $idDireccion) {
      $queryUpdate = $this->db->query("update PERSONAS
                                  set nombre = '$nombre',
                                      apellido = '$apellido',
                                      dni = $dni,
                                      fecha_nacimiento = '$fechaNac',
                                      codigo_direccion = $idDireccion
                                      where codigo = $idPersona;");

      $query = $this->db->query("SELECT MAX(codigo) as idPersona from PERSONAS;");
		
	  return $query->result();
	}
}