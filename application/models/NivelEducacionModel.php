<?php

class NivelEducacionModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getNiveles() {
		$query = $this->db->query("select ni.codigo, ni.descripcion
									from NIVEL_EDUCACION ni");
		return $query->result();
	}

	public function getNivel($id) {
		$query = $this->db->query("select ni.codigo, ni.descripcion
									from NIVEL_EDUCACION ni 
									where ni.codigo = $id");
		return $query->result();
	}
	
	
	
}