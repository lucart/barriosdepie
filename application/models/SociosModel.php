<?php

class SociosModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getSocios() {
		$query = $this->db->query("select * from SOCIOS");
		return $query->result();
	}

	public function getSocio($id) {
		$query = $this->db->query("select * from SOCIOS where codigo = $id");
		return $query->result();
	}
	
	public function getSociosBeneficio($benefit) {
		$query = $this->db->query("SELECT so.nro_socio nrosocio, (
											SELECT CONCAT(pe.nombre,' ', pe.apellido) 
											FROM PERSONAS pe, MAYOR ma
											WHERE ma.codigo_socio = so.codigo
											AND ma.codigo_persona = pe.codigo
											)nombreMa, (
											SELECT CONCAT(pe.nombre,' ', pe.apellido) 
											FROM PERSONAS pe, COORDINADOR co
											WHERE co.codigo_socio = so.codigo
											AND co.codigo_persona = pe.codigo
											)nombreCo,
									so.cuil cuil, be.nombre beneficio
									FROM BENEFICIOSOCIO bs
									INNER JOIN SOCIOS so ON so.codigo = bs.codigo_socio
									INNER JOIN BENEFICIO be ON be.codigo = bs.codigo_beneficio
									WHERE bs.codigo_beneficio = '$benefit'");
		return $query->result();
	}
	
	public function getSociosBeneficios() {
		$query = $this->db->query("SELECT so.nro_socio nrosocio, (
											SELECT CONCAT(pe.nombre,' ', pe.apellido) 
											FROM PERSONAS pe, MAYOR ma
											WHERE ma.codigo_socio = so.codigo
											AND ma.codigo_persona = pe.codigo
											)nombreMa, (
											SELECT CONCAT(pe.nombre,' ', pe.apellido) 
											FROM PERSONAS pe, COORDINADOR co
											WHERE co.codigo_socio = so.codigo
											AND co.codigo_persona = pe.codigo
											)nombreCo,
									so.cuil cuil, be.nombre beneficio
									FROM BENEFICIOSOCIO bs
									INNER JOIN SOCIOS so ON so.codigo = bs.codigo_socio
									INNER JOIN BENEFICIO be ON be.codigo = bs.codigo_beneficio");
		return $query->result();
	}
	
	public function insertSocio($number, $cuil) {
		$queryIns = $this->db->query("INSERT INTO `SOCIOS` ( `nro_socio`, `cuil`) 
									VALUES ('$number',$cuil);");								
		$query = $this->db->query("SELECT MAX(codigo) as idSocio from SOCIOS;");
		
		return $query->result();
	}
	
	public function updateSocio($numberSoc, $cuil, $idSocio) {
      $query = $this->db->query("update SOCIOS
                                  set nro_socio = '$numberSoc',
                                      cuil = $cuil
                                      where codigo = $idSocio;");
      return true;
    }
}