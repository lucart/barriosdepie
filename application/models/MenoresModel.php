<?php

class MenoresModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getMenores() {
		$query = $this->db->query("select m.codigo, p.apellido, p.nombre, p.dni, p.fecha_nacimiento, map.nombre nombreTutor, map.apellido apellidoTutor, c.nombre comedor
			from MENOR m, PERSONAS p, MAYOR ma, COMEDOR c, PERSONAS map
			where m.codigo_persona = p.codigo
			and m.codigo_tutor = ma.codigo
			and map.codigo = ma.codigo_persona
			and m.codigo_comedor = c.codigo");
		return $query->result();
	}

	public function getMenor($id) {
		$query = $this->db->query("select m.codigo, p.apellido, 
			p.nombre, p.dni, p.fecha_nacimiento, m.codigo_tutor, 
			m.codigo_comedor, l.codigo_provincia, b.codigo_localidad,
			d.codigo_barrio, d.codigo_postal, d.calle, d.numero, m.codigo_persona,
			p.codigo_direccion
			from MENOR m, PERSONAS p, DIRECCION d, BARRIO b, LOCALIDAD l
			where m.codigo_persona = p.codigo
			and p.codigo_direccion = d.codigo
			and d.codigo_barrio = b.codigo
			and b.codigo_localidad = l.codigo
			and m.codigo = $id");
		return $query->result();
	}
	
	public function insertMenor($idPersona, $idComedor, $idTutor) {
		$query = $this->db->query("INSERT INTO `MENOR` (codigo_persona, codigo_comedor, codigo_tutor) 
									VALUES ($idPersona, $idComedor, $idTutor);");
		return true;
	}

	public function updateMenor($idMenor, $idPersona, $idComedor, $idTutor) {
      $query = $this->db->query("update MENOR
                                  set codigo_persona = $idPersona,
                                      codigo_comedor = $idComedor,
                                      codigo_tutor = $idTutor
                                      where codigo = $idMenor;");
      return true;
    }
	
}