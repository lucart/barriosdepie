<?php

class ComedorModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getComedores() {
		$query = $this->db->query("select co.codigo, co.nombre, co.observacion, concat(pe.nombre, ' ', pe.apellido) coordinador 
									from `COMEDOR` co, `COORDINADOR` coo, `PERSONAS` pe
									where co.codigo_cordinador = coo.codigo and coo.codigo_persona = pe.codigo");
		return $query->result();
	}

	public function getComedor($id) {
		$query = $this->db->query("select codigo, nombre, observacion, codigo_cordinador from `COMEDOR` where codigo = $id");
		return $query->result();
	}
	
	public function insertComedor($nombre, $coordinador, $observacion) {
		$query = $this->db->query("INSERT INTO `COMEDOR` ( `nombre`, `codigo_cordinador`, `observacion`) 
									VALUES ('$nombre','$coordinador','$observacion');");
		return true;
	}

	public function deleteComedor($id) {
      $query = $this->db->query("delete from COMEDOR where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }

    public function updateComedor($nombre, $coordinador, $observacion, $idComedor) {
      $query = $this->db->query("update COMEDOR
                                  set nombre = '$nombre',
                                      codigo_cordinador = $coordinador,
                                      observacion = '$observacion'
                                      where codigo = $idComedor;");
      return true;
    }
}