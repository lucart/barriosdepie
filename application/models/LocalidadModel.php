<?php

class LocalidadModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getLocalidades() {
		$query = $this->db->query("select lo.codigo, lo.nombre, lo.codigo_provincia
									from LOCALIDAD lo");
		return $query->result();
	}

	public function getLocalidad($id) {
		$query = $this->db->query("select lo.codigo, lo.nombre, lo.codigo_provincia
									from LOCALIDAD lo
									where lo.codigo = $id ");
		return $query->result();
	}
	
	public function getLocalidadesProvincia($idProvincia) {
		$query = $this->db->query("select lo.codigo, lo.nombre
									from LOCALIDAD lo
									where lo.codigo_provincia = $idProvincia ");
		return $query->result();
	}
	
	
}