<?php

class MayoresModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getMayores() {
		$query = $this->db->query("select ma.codigo, ma.codigo_persona, pe.nombre, pe.apellido, pe.dni, pe.edad
									from MAYOR ma, PERSONAS pe
									where ma.codigo_persona = pe.codigo");
		return $query->result();
	}

	public function getMayor($id) {
		$query = $this->db->query("select ma.codigo, ma.codigo_persona, ma.codigo_socio, pe.nombre, pe.apellido, pe.dni, 
									pe.edad, pe.fecha_nacimiento, pe.codigo_direccion, ma.trabajo_anterior, ma.trabajo_actual,
									ma.telefono, ma.email, ma.facebook, ma.cant_pers_vivienda, ma.cant_hijos_cargo, ma.oficio, ma.curso_oficio,
									ma.codigo_nivel_estudio, ma.codigo_vivienda
									from MAYOR ma, PERSONAS pe 
									where ma.codigo = $id and ma.codigo_persona = pe.codigo");
		return $query->result();
	}
								
	public function insertMayor($phone, $email, $facebook, $idPreviousWork, $idCurrentWork, $ofice, $cantchild, $canthome,$idPersona,$idSocio, $level_education, $oficce_course, $vivienda) {
		$query = $this->db->query("INSERT INTO `MAYOR` (`telefono`, `email`, `facebook`, `trabajo_anterior`, `trabajo_actual`, `oficio`, `cant_hijos_cargo`, `cant_pers_vivienda`, `codigo_persona`, `codigo_socio`, `codigo_nivel_estudio`, `curso_oficio`, `codigo_vivienda`) 
									VALUES ('$phone', '$email', '$facebook', '$idPreviousWork', '$idCurrentWork', '$ofice', '$cantchild', '$canthome','$idPersona','$idSocio','$level_education','$oficce_course','$vivienda');");
		return true;
	}
	
	 public function updateMayor($phone, $email, $facebook, $idPreviousWork, $idCurrentWork, $ofice, $cantchild, $canthome,$idPersona,$idSocio, $level_education, $oficce_course, $vivienda, $idMayor) {
      $query = $this->db->query("update MAYOR
                                  set telefono = '$phone',
                                      email = '$email',
                                      facebook = '$facebook',
                                      trabajo_anterior = $idPreviousWork,
                                      trabajo_actual = $idCurrentWork,
                                      oficio = '$ofice',
                                      cant_hijos_cargo = $cantchild,
                                      cant_pers_vivienda = $canthome,
                                      codigo_persona = $idPersona,
                                      codigo_socio = $idSocio,
                                      codigo_nivel_estudio = $level_education,
                                      curso_oficio = '$oficce_course',
                                      codigo_vivienda = $vivienda
                                      where codigo = $idMayor;");
      return true;
    }
	
}