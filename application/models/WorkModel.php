<?php

class WorkModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getWorks() {
		$query = $this->db->query("select * from TRABAJO");
		return $query->result();
	}

	public function getWork($id) {
		$query = $this->db->query("select * from TRABAJO where codigo = $id");
		return $query->result();
	}
	
	public function insertWork($desc, $fechaIni, $fechaFin) {
		$queryIns = $this->db->query("INSERT INTO `TRABAJO` ( `descripcion`, `fecha_inicio`,`fecha_fin`) 
									VALUES ('$desc','$fechaIni','$fechaFin');");								
		$query = $this->db->query("SELECT MAX(codigo) as idWork from TRABAJO;");
		
		return $query->result();
	}
	
	public function updateWork($desc, $fechaIni, $fechaFin,$idWork) {
      $query = $this->db->query("update TRABAJO
                                  set descripcion = '$desc',
                                      fecha_inicio = '$fechaIni',
                                      fecha_fin = '$fechaFin'
                                      where codigo = $idWork;");
      return true;
    }
}