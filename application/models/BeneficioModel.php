<?php

class BeneficioModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getBeneficios() {
		$query = $this->db->query("select * from `BENEFICIO`");
		return $query->result();
	}

	public function getBeneficio($id) {
		$query = $this->db->query("select * from `BENEFICIO` where codigo = $id");
		return $query->result();
	}
	
	public function getBeneficioSocio($idSocio) {
		$query = $this->db->query("SELECT be.codigo, bs.observacion, bs.otros_beneficios, bs.fecha_inicio, bs.fecha_fin
									FROM BENEFICIOSOCIO bs
									INNER JOIN BENEFICIO be ON be.codigo = bs.codigo_beneficio
									WHERE bs.codigo_socio = $idSocio");
		return $query->result();
	}
	
	public function insertBeneficioSocio($benefit, $idSocio, $otherBenefit, $dateStart, $dateEnd, $observation) {
		$query = $this->db->query("INSERT INTO `BENEFICIOSOCIO` ( `codigo_beneficio`, `codigo_socio`, `otros_beneficios`, `observacion`, `fecha_inicio`, `fecha_fin`) 
									VALUES ('$benefit','$idSocio','$otherBenefit','$observation', '$dateStart', '$dateEnd');");
		
		return true;
	}
	
	public function deleteBeneficioSocio($idSocio) {
		$query = $this->db->query("DELETE FROM `BENEFICIOSOCIO` WHERE codigo_socio = $idSocio;");
		
		return true;
	}
}