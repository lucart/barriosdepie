<?php

class CoordinadorModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getCoordinadores() {
		$query = $this->db->query("select co.codigo, co.codigo_persona, co.codigo_socio, pe.nombre, pe.apellido, pe.dni, pe.edad
									from COORDINADOR co, PERSONAS pe
									where co.codigo_persona = pe.codigo");
		return $query->result();
	}

	public function getCoordinador($id) {
		$query = $this->db->query("select co.codigo, co.codigo_persona, co.codigo_socio, pe.nombre, pe.apellido, pe.dni, pe.edad, pe.fecha_nacimiento, pe.codigo_direccion
									from COORDINADOR co, PERSONAS pe 
									where co.codigo = $id and co.codigo_persona = pe.codigo");
		return $query->result();
	}
	
	public function insertCoordinador($idPersona,$idSocio) {
		$query = $this->db->query("INSERT INTO `COORDINADOR` ( `codigo_persona`, `codigo_socio`) 
									VALUES ('$idPersona','$idSocio');");
		return true;
	}
	
}