<?php

class BarrioModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getBarrios() {
		$query = $this->db->query("select ba.codigo, ba.nombre, ba.codigo_localidad
									from BARRIO ba");
		return $query->result();
	}

	public function getBarrio($id) {
		$query = $this->db->query("select ba.codigo, ba.nombre, ba.codigo_localidad
									from BARRIO ba
									where ba.codigo = $id ");
		return $query->result();
	}
	
	public function getBarriosLocalidad($idLocalidad) {
		$query = $this->db->query("select ba.codigo, ba.nombre
									from BARRIO ba
									where ba.codigo_localidad = $idLocalidad ");
		return $query->result();
	}
	
	
}