<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function validateSessionData($session){
		if($session)
		{
			$session_data = $session;
			$data['username'] = $session_data['username'];
			$data['nombre'] = $session_data['nombre'];
			$data['apellido'] = $session_data['apellido'];
			$data['email'] = $session_data['email'];
			$data['permiso'] = $session_data['permiso'];

			return $data;
		}
		else
		{
			//If no session, redirect to login page
			redirect('LoginController/index', 'refresh');
		}
	}   
