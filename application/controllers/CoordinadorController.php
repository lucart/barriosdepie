<?php

class CoordinadorController extends CI_Controller {

	function __construct(){
		parent::__construct();
		//Comunicacion con el modelo
		$this->load->model('CoordinadorModel');
		$this->load->model('PersonasModel');
		$this->load->model('SociosModel');
		$this->load->model('ProvinciaModel');
		$this->load->model('LocalidadModel');
		$this->load->model('BarrioModel');
		$this->load->model('DireccionModel');
		$this->load->model('BeneficioModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "coordinador/lista";
		$data['coordinadores'] = $this->CoordinadorModel->getCoordinadores();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "coordinador/create";
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['barrios'] = $this->BarrioModel->getBarrios();
		$data['beneficios'] = $this->BeneficioModel->getBeneficios();
		$this->load->view("template/template", $data);
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idCoordinador = $_GET["idCoordinador"];
		$data['contenido'] = "coordinador/update";
		$data['coordinador'] = $this->CoordinadorModel->getCoordinador($idCoordinador);
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['barrios'] = $this->BarrioModel->getBarrios();
		$data['direccion'] = $this->DireccionModel->getDireccion($data['coordinador'][0]->codigo_direccion);
		$data['barrio'] = $this->BarrioModel->getBarrio($data['direccion'][0]->codigo_barrio);
		$data['localidad'] = $this->LocalidadModel->getLocalidad($data['barrio'][0]->codigo_localidad);
		$data['provincia'] = $this->ProvinciaModel->getProvincia($data['localidad'][0]->codigo_provincia);
		$data['socio'] = $this->SociosModel->getSocio($data['coordinador'][0]->codigo_socio);
		$data['beneficios'] = $this->BeneficioModel->getBeneficios();
		$data['beneficioSocio'] = $this->BeneficioModel->getBeneficioSocio($data['coordinador'][0]->codigo_socio);
		$this->load->view("template/template", $data);
	}

	public function eliminarComedor(){
		$idComedor = $_GET["idComedor"];
		$this->ComedorModel->deleteComedor($idComedor);

		redirect(base_url() . 'index.php/ComedorController/lista');
	}

	
	public function guardarCoordinador(){
		
		
		$idCoordinador = $_POST["idCoordinador"];
		$idPersona = $_POST["idPersona"];
		$idDireccion = $_POST["idDireccion"];
		$idSocio = $_POST["idSocio"];
		
		//Getting address data by post
		$street = $_POST["street"];
		$number = $_POST["number"];
		$barrio = $_POST["barrio"];
		$postalCode = $_POST["postalCode"];
		if($idDireccion != null){
			$this->DireccionModel->updateDireccion($idDireccion,$street, $number, $postalCode, $barrio);
		}else{			
			$idDireccion = $this->DireccionModel->insertDireccion($street, $number, $postalCode, $barrio);
		}
		
		
		//Getting person data by post
		$name = $_POST["name"];
		$surname = $_POST["surname"];
		$dni = $_POST["dni"];
		$birthdate = $_POST["birthdate"];
		$age = $this->calculaedad($birthdate);	
		
		if($idPersona != null){
			$this->PersonasModel->updatePersona($idPersona, $name, $surname, $dni, $birthdate, $idDireccion);
		}else{		
			$idPersona = $this->PersonasModel->insertPersona($name, $surname, $dni, $birthdate, $idDireccion [0]->idDireccion);
		}
		
		
		//Getting partner data by post
		$numberSoc = $_POST["nroScio"];
		$cuil = $_POST["cuil"];
		
		if($idSocio != null){
			$this->SociosModel->updateSocio($numberSoc, $cuil, $idSocio);
		}else{		
			$idSocio = $this->SociosModel->insertSocio($numberSoc, $cuil);
		}
				
		
		
		//Getting benefit data by post
		$benefitIni = $_POST["BenefitIni"];
		$benefitFin = $_POST["BenefitFin"];
		
		
		if($idCoordinador != NULL){
			$this->BeneficioModel->deleteBeneficioSocio($idSocio);
		}
		
				
		for($i = $benefitIni; $i <= $benefitFin; $i++){
			
			$benefit = $_POST["benefit_".$i];
			if($benefit != NULL){
				$otherBenefit = $_POST["otherBenefit_".$i];
				$dateStart = $_POST["dateStart_".$i];
				$dateEnd = $_POST["dateEnd_".$i];
				$observation = $_POST["observation_".$i];
				if($idCoordinador != NULL){
					$this->BeneficioModel->insertBeneficioSocio($benefit, $idSocio, $otherBenefit, $dateStart, $dateEnd, $observation);
				}else{
					$this->BeneficioModel->insertBeneficioSocio($benefit, $idSocio[0]->idSocio, $otherBenefit, $dateStart, $dateEnd, $observation);
				}
			}
			
		}
		
		if($idCoordinador == null){
			$this->CoordinadorModel->insertCoordinador($idPersona [0]->idPersona, $idSocio[0]->idSocio);
		}
		
		
		redirect(base_url() . 'index.php/CoordinadorController/lista');
	}
	
	
	function calculaedad($fecha){
		$date2 = date('Y-m-d');//
		$diff = abs(strtotime($date2) - strtotime($fecha));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		return $years;
	}
}