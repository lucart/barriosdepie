<?php

class MayorController extends CI_Controller {

	function __construct(){
		parent::__construct();
		//Comunicacion con el modelo
		$this->load->model('MayoresModel');
		$this->load->model('PersonasModel');
		$this->load->model('SociosModel');
		$this->load->model('NivelEducacionModel');
		$this->load->model('ProvinciaModel');
		$this->load->model('LocalidadModel');
		$this->load->model('BarrioModel');
		$this->load->model('DireccionModel');
		$this->load->model('WorkModel');
		$this->load->model('BeneficioModel');
		$this->load->model('ViviendaModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "mayores/lista";
		$data['mayores'] = $this->MayoresModel->getMayores();	
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "mayores/create";
		$data['niveles'] = $this->NivelEducacionModel->getNiveles();
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['barrios'] = $this->BarrioModel->getBarrios();
		$data['beneficios'] = $this->BeneficioModel->getBeneficios();
		$data['viviendas'] = $this->ViviendaModel->getViviendas();
		$data['propietario'] = $this->ViviendaModel->getViviendasPropietario();
		$this->load->view("template/template", $data);
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idMayor = $_GET["idMayor"];
		$data['contenido'] = "mayores/update";
		$data['mayores'] = $this->MayoresModel->getMayor($idMayor);
		$data['niveles'] = $this->NivelEducacionModel->getNiveles();
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['barrios'] = $this->BarrioModel->getBarrios();
		$data['beneficios'] = $this->BeneficioModel->getBeneficios();
		$data['viviendas'] = $this->ViviendaModel->getViviendas();
		$data['propietario'] = $this->ViviendaModel->getViviendasPropietario();
		$data['direccion'] = $this->DireccionModel->getDireccion($data['mayores'][0]->codigo_direccion);
		$data['barrio'] = $this->BarrioModel->getBarrio($data['direccion'][0]->codigo_barrio);
		$data['localidad'] = $this->LocalidadModel->getLocalidad($data['barrio'][0]->codigo_localidad);
		$data['provincia'] = $this->ProvinciaModel->getProvincia($data['localidad'][0]->codigo_provincia);
		$data['socio'] = $this->SociosModel->getSocio($data['mayores'][0]->codigo_socio);
		$data['trabajoAnt'] = $this->WorkModel->getWork($data['mayores'][0]->trabajo_anterior);
		$data['trabajoAct'] = $this->WorkModel->getWork($data['mayores'][0]->trabajo_actual);
		$data['beneficioSocio'] = $this->BeneficioModel->getBeneficioSocio($data['mayores'][0]->codigo_socio);
		$this->load->view("template/template", $data);
	}

	public function guardarMayor(){
		
		$idMayor = $_POST["idMayor"];
		$idPersona = $_POST["idPersona"];
		$idDireccion = $_POST["idDireccion"];
		$idSocio = $_POST["idSocio"];
		$idPreviousWork = $_POST["idPreviousWork"];
		$idCurrentWork = $_POST["idCurrentWork"];
		
		
		//Getting address data by post
		$street = $_POST["street"];
		$number = $_POST["number"];
		$barrio = $_POST["barrio"];
		$postalCode = $_POST["postalCode"];
		
		if($idDireccion != null){
			$this->DireccionModel->updateDireccion($idDireccion, $street, $number, $postalCode, $barrio);
		}else{			
			$idDireccion = $this->DireccionModel->insertDireccion($street, $number, $postalCode, $barrio);
		}
		
		//Getting person data by post
		$name = $_POST["name"];
		$surname = $_POST["surname"];
		$dni = $_POST["dni"];	
		$birthdate = $_POST["birthdate"];
		
		if($idPersona != null){
			$this->PersonasModel->updatePersona($idPersona, $name, $surname, $dni, $birthdate, $idDireccion);
		}else{		
			$idPersona = $this->PersonasModel->insertPersona($name, $surname, $dni, $birthdate, $idDireccion [0]->idDireccion);
		}
		
		//Getting partner data by post
		$numberSoc = $_POST["nroScio"];
		$cuil = $_POST["cuil"];
		
		if($idSocio != null){
			$this->SociosModel->updateSocio($numberSoc, $cuil, $idSocio);
		}else{		
			$idSocio = $this->SociosModel->insertSocio($numberSoc, $cuil);
		}		
		
		
		//Getting benefit data by post
		$benefitIni = $_POST["BenefitIni"];
		$benefitFin = $_POST["BenefitFin"];
		if(idMayor){
			$this->BeneficioModel->deleteBeneficioSocio($idSocio);
		}
		
				
		for($i = $benefitIni; $i <= $benefitFin; $i++){
			
			$benefit = $_POST["benefit_".$i];
			if($benefit != NULL){
				$otherBenefit = $_POST["otherBenefit_".$i];
				$dateStart = $_POST["dateStart_".$i];
				$dateEnd = $_POST["dateEnd_".$i];
				$observation = $_POST["observation_".$i];
				if($idMayor != null){
					$this->BeneficioModel->insertBeneficioSocio($benefit, $idSocio, $otherBenefit, $dateStart, $dateEnd, $observation);
				}else{
					$this->BeneficioModel->insertBeneficioSocio($benefit, $idSocio[0]->idSocio, $otherBenefit, $dateStart, $dateEnd, $observation);
				}
			}
			
		}
		
		//Getting previous work data by post
		
		$descTraAnt = $_POST["descTraAnt"];
		$fechaIniAnt = $_POST["fechaIniAnt"];
		$fechaFinAnt = $_POST["fechaFinAnt"];
		
		if($idPreviousWork != null){
			$this->WorkModel->updateWork($descTraAnt, $fechaIniAnt, $fechaFinAnt,$idPreviousWork);
		}else{				
			$idPreviousWork = $this->WorkModel->insertWork($descTraAnt, $fechaIniAnt, $fechaFinAnt);
		}
		
		//Getting current work data by post
		
		$descTraAct = $_POST["descTraAct"];
		$fechaIniAct = $_POST["fechaIniAct"];
		
		if($idCurrentWork != null){
			$this->WorkModel->updateWork($descTraAct, $fechaIniAct, NULL,$idCurrentWork);
		}else{	
			$idCurrentWork = $this->WorkModel->insertWork($descTraAct, $fechaIniAct, NULL);
		}
		
		
		//Getting major data by post		
		
		$phone = $_POST["phone"];
		$email = $_POST["email"];
		$facebook = $_POST["facebook"];
		$level_education = $_POST["level_education"];
		$oficce_course = $_POST["oficce_course"];
		$ofice = $_POST["ofice"];	
		$cantchild = $_POST["cantchild"];	
		$canthome = $_POST["canthome"];
		if($_POST["living_place"]==0){
			$vivienda = NULL;	
		}else{
			$vivienda = $_POST["living_place"];	
		}
		
		if($vivienda == 3){
			$vivienda=$_POST["owner"];
		}
		
		if($idMayor != null){
			$this->MayoresModel->updateMayor($phone, $email, $facebook, $idPreviousWork, $idCurrentWork, $ofice, $cantchild, $canthome, $idPersona, $idSocio, $level_education, $oficce_course, $vivienda, $idMayor);
		}else{	
			$this->MayoresModel->insertMayor($phone, $email, $facebook, $idPreviousWork[0]->idWork, $idCurrentWork[0]->idWork, $ofice, $cantchild, $canthome, $idPersona [0]->idPersona, $idSocio[0]->idSocio, $level_education, $oficce_course, $vivienda);
		}
		
		
		redirect(base_url() . 'index.php/MayorController/lista');		
		
	}
	
	
	function calculaedad($fecha){
		$date2 = date('Y-m-d');//
		$diff = abs(strtotime($date2) - strtotime($fecha));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		return $years;
	}
}