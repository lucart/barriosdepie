<?php

class MenorController extends CI_Controller {

	function __construct(){
		parent::__construct();
		//Comunicacion con el modelo
		$this->load->model('MenoresModel');
		$this->load->model('PersonasModel');
		$this->load->model('ProvinciaModel');
		$this->load->model('LocalidadModel');
		$this->load->model('BarrioModel');
		$this->load->model('DireccionModel');
		$this->load->model('MayoresModel');
		$this->load->model('ComedorModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "menores/lista";
		$data['menores'] = $this->MenoresModel->getMenores();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "menores/create";
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['barrios'] = $this->BarrioModel->getBarrios();
		$data['mayores'] = $this->MayoresModel->getMayores();
		$data['comedores'] = $this->ComedorModel->getComedores();
		$this->load->view("template/template", $data);
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idMenor = $_GET["idMenor"];
		$data['contenido'] = "menores/update";
		$data['menor'] = $this->MenoresModel->getMenor($idMenor);
		$data['mayores'] = $this->MayoresModel->getMayores();
		$data['comedores'] = $this->ComedorModel->getComedores();
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['barrios'] = $this->BarrioModel->getBarrios();
		$this->load->view("template/template", $data);
	}

	public function guardarMenor(){
		$idMenor = $_POST["idMenor"];
		$idPersona = $_POST["idPersona"];
		$idDireccion = $_POST["idDireccion"];

		$calle = $_POST["calle"];
		$numero = $_POST["numero"];
		$codigoPostal = $_POST["codigoPostal"];
		$barrio = $_POST["barrio"];

		$nombre = $_POST["nombre"];
		$apellido = $_POST["apellido"];
		$dni = $_POST["dni"];	
		$fechaNac = $_POST["fechaNac"];		
		
		$idTutor = $_POST["tutor"];
		$idComedor = $_POST["comedor"];

		if($idMenor != null){
			$idDireccion = $this->DireccionModel->updateDireccion($idDireccion, $calle, $numero, $codigoPostal, $barrio);
			$idPersona = $this->PersonasModel->updatePersona($idPersona, $nombre, $apellido, $dni, $fechaNac, $idDireccion[0]->idDireccion);
			$this->MenoresModel->updateMenor($idMenor, $idPersona[0]->idPersona, $idComedor, $idTutor);
		}else{
			$idDireccion = $this->DireccionModel->insertDireccion($calle, $numero, $codigoPostal, $barrio);
			$idPersona = $this->PersonasModel->insertPersona($nombre, $apellido, $dni, $fechaNac, $idDireccion[0]->idDireccion);
			$this->MenoresModel->insertMenor($idPersona[0]->idPersona, $idComedor, $idTutor);
		}		
	}
}