<?php

class SocioController extends CI_Controller {

	function __construct(){
		parent::__construct();
		//Comunicacion con el modelo
		$this->load->model('SociosModel');
		$this->load->model('BeneficioModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function socioPorBeneficio(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "socios/reporte";
		$data['beneficios'] = $this->BeneficioModel->getBeneficios();
		$data['socios'] = $this->SociosModel->getSociosBeneficios();
		$this->load->view("template/template", $data);
	}
	
		
	public function socioPorBeneficioRefresh(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$benefit = $_GET["benefit"];
		$data['contenido'] = "socios/reporte";
		$data['beneficios'] = $this->BeneficioModel->getBeneficios();	
		if($benefit==0){			
			$data['socios'] = $this->SociosModel->getSociosBeneficios();
		}else{
			$data['socios'] = $this->SociosModel->getSociosBeneficio($benefit);	
		}
		$this->load->view("template/template", $data);
	}
	
}