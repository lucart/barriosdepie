<?php

class UsuarioController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('UsuariosModel');
		$this->load->model('PermisosModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "usuario/lista";
		$data['usuarios'] = $this->UsuariosModel->getUsuarios();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "usuario/create";
		$data['permisos'] = $this->PermisosModel->getPermisos();
		$this->load->view("template/template", $data);
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idUsuario = $_GET["idUsuario"];
		$data['contenido'] = "usuario/update";
		$data['usuario'] = $this->UsuariosModel->getUsuario($idUsuario);
		$data['permisos'] = $this->PermisosModel->getPermisos();
		$this->load->view("template/template", $data);
	}

	public function guardarUsuario(){
		$nombre = $_POST["nombre"];
		$apellido = $_POST["apellido"];
		$email = $_POST["email"];
		$username = $_POST["username"];
		$password = $_POST["password"];
		$idPermiso = $_POST["permiso"];
		$idUsuario = $_POST["idUsuario"];

		if($idUsuario != null){
			if($this->cambioPassword($idUsuario, $password)){
				$this->UsuariosModel->updateUsuario($nombre, $apellido, $email, $username, $password, $idPermiso, $idUsuario);
			}else{
				$this->UsuariosModel->updateUsuarioSinPassword($nombre, $apellido, $email, $username, $idPermiso, $idUsuario);
			}
		}else{
			$this->UsuariosModel->insertUsuario($nombre, $apellido, $email, $username, $password, $idPermiso);
		}

		redirect(base_url() . 'index.php/UsuarioController/lista');
	}

	public function eliminarUsuario(){
		$idUsuario = $_GET["idUsuario"];
		$this->UsuariosModel->deleteUsuario($idUsuario);

		redirect(base_url() . 'index.php/UsuarioController/lista');
	}

	private function cambioPassword($idUsuario, $password){
		$data = $this->UsuariosModel->getPasswordByUserId($idUsuario);
		if($password == $data[0]->password){
			return false;
		} else {
			return true;
		}
	}
}