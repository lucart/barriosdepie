<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HomeController extends CI_Controller {

   function __construct()
   {
     parent::__construct();
     error_reporting(E_ERROR | E_PARSE);
     if(empty($this->session->userdata("logged_in")))
      {
        redirect('LoginController/index', 'refresh');
      }
   }

   function index()
   {
     if($this->session->userdata('logged_in'))
     {
       $session_data = $this->session->userdata('logged_in');
       $data['contenido'] = "home";
       $data['username'] = $session_data['username'];
       $data['nombre'] = $session_data['nombre'];
       $data['apellido'] = $session_data['apellido'];
       $data['email'] = $session_data['email'];
       $data['permiso'] = $session_data['permiso'];
       $this->load->view("template/template", $data);
     }
     else
     {
       //If no session, redirect to login page
       redirect('LoginController/index', 'refresh');
     }
   }

   function logout()
   {
     $this->session->unset_userdata('logged_in');
     session_destroy();
     redirect('LoginController/index', 'refresh');
   }

}

?>