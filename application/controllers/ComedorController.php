<?php

class ComedorController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('ComedorModel');
		$this->load->model('CoordinadorModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
	    {
	      redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "comedor/lista";
		$data['comedores'] = $this->ComedorModel->getComedores();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "comedor/create";
		$data['coordinadores'] = $this->CoordinadorModel->getCoordinadores();
		$this->load->view("template/template", $data);
	}
	
	public function guardarComedor(){
		$nombre = $_POST["nombre"];
		$coordinador = $_POST["coordinador"];
		$observacion = $_POST["observacion"];
		$idComedor = $_POST["idComedor"];

		if($idComedor != null){
			$this->ComedorModel->updateComedor($nombre, $coordinador, $observacion, $idComedor);
		}else{
			$this->ComedorModel->insertComedor($nombre, $coordinador, $observacion);
		}
				
		redirect(base_url() . 'index.php/ComedorController/lista');
	}

	public function eliminarComedor(){
		$idComedor = $_GET["idComedor"];
		$this->ComedorModel->deleteComedor($idComedor);

		redirect(base_url() . 'index.php/ComedorController/lista');
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idComedor = $_GET["idComedor"];
		$data['contenido'] = "comedor/update";
		$data['comedor'] = $this->ComedorModel->getComedor($idComedor);
		$data['coordinadores'] = $this->CoordinadorModel->getCoordinadores();
		$this->load->view("template/template", $data);
	}

}