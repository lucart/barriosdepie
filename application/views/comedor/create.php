<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Crear un Nuevo Comedor</h3>
  </div>
  <div class="box-body">
    <form id="guardarComedorForm" role="form" action="../ComedorController/guardarComedor" method="post" >		
	  <table class="table table-hover">
		<tr>
		  <td width="25%">		  
			  <div class="form-group">
				<label>Nombre</label>
				<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ingrese el Nombre del comedor">
			  </div>
		  </td>
		  <td width="5%"></td>
		  <td width="25%">
			  <div class="form-group">
				<label>Coordinador/a</label>
				<select id="coordinador" name="coordinador" class="form-control" >	
					<option  value="0">Seleccione...</option>						
					<?php
						for ($i = 0; $i < count($coordinadores); $i++) {
					?>
					<option  value="<?php echo $coordinadores[$i]->codigo?>"><?php echo $coordinadores[$i]->nombre?></option>
					<?php } ?>
				</select>
			  </div>
		  </td>
		  <td width="45%"></td>
		</tr>
		<tr>
		  <td colspan="3">
			  <div class="form-group">
				<label>Observación</label>
				<textarea class="form-control" id="observacion" name="observacion" rows="5" placeholder="Ingrese la observación"></textarea>
			  </div>
		  </td>
		</tr>
	  </table>
      <div class="box-footer">
        <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateComedor.js')?>"></script>