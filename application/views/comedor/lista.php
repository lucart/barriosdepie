<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado De Comedores</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Coordinador</th>
                  <th>Observación</th>
                  <th>Acciones</th>
                </tr>                
          				<?php
          					for ($i = 0; $i < count($comedores); $i++) {		
          				?>
                <tr>
                  <td><?php echo $comedores[$i]->codigo;?></td>
                  <td><?php echo $comedores[$i]->nombre;?></td>
                  <td><?php echo $comedores[$i]->coordinador;?></td>
                  <td><?php echo $comedores[$i]->observacion;?></td>
                  <td><button onclick="window.location.href='../ComedorController/update?idComedor=<?php echo $comedores[$i]->codigo;?>'" class="btn btn-warning glyphicon glyphicon-pencil btn-xs"></button>
                        <button onclick="confirmDelete(<?php echo $comedores[$i]->codigo;?>);" class="btn btn-danger glyphicon glyphicon-trash btn-xs" style="margin-left: 10px;"></button>
                  </td>
                </tr>
				<?php } ?>
              </table>
            </div>
          </div>
        </div>
</div>
<script src="<?php echo base_url('public/js/validateComedor.js')?>"></script>