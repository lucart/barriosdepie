<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado De Socios</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <td width="25%">				
					  <div class="form-group">
						<label>Beneficio</label>
						<select id="benefit" name="benefit" class="form-control" onchange="filtroBeneficio();" >
							<option  value="0">Todos</option>
							<?php
								for ($i = 0; $i < count($beneficios); $i++) {
								

							?>
							<option  value="<?php echo $beneficios[$i]->codigo?>"><?php echo $beneficios[$i]->nombre?></option>
							<?php } ?>
						</select>
					  </div>
					</td>
					<td width="25%"></td>
					<td width="25%"></td>
					<td width="25%"></td>
                </tr>
              </table>
              <table id="table" class="table table-hover">
                <tr>
                  <th>Número Socio</th>
                  <th>Nombre</th>
                  <th>Cuil</th>
                  <th>Beneficio</th>
                </tr>
				<?php
					for ($i = 0; $i < count($socios); $i++) {					

				?>
                <tr>
                  <td><?php echo $socios[$i]->nrosocio;  ?></td>
                  <td><?php if($socios[$i]->nombreMa!= null){ echo $socios[$i]->nombreMa.' '.$socios[$i]->apellidoMa;}else{ echo $socios[$i]->nombreCo.' '.$socios[$i]->apellidoCo;}  ?></td>
                  <td><?php echo $socios[$i]->cuil;  ?></td>
                  <td><?php echo $socios[$i]->beneficio;  ?></td>
                </tr>
				<?php } ?>
                <tr>
                  <td width="25%"></td>
                  <td width="45%"></td>
                  <td width="15%"><b>Total Beneficiarios: </b></td>
                  <td width="15%"><b><?php echo $i;  ?></b></td>
                </tr>
              </table>	
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
	 
<script>
	var URLactual = window.location;
	var benefitActual = String(URLactual).split("=");
	if(benefitActual[1]=='' || benefitActual[1]==null){
		benefitActual[1]=0;
	}
	document.getElementById("benefit").value=benefitActual[1];
	
	function filtroBeneficio(){
		
		var benefit=document.getElementById("benefit").value;
		location.replace("../SocioController/socioPorBeneficioRefresh?benefit="+benefit);
		/*
		
		 $.ajax({
			url: "socioPorBeneficioRefresh",
			type: 'POST',
			data: { "benefit" :  benefit },
			success: function (data) {
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
		});*/
	}
	
	
	
</script>
	  
	  