<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Coordinador/a</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="guardarCoordinadorForm" role="form" action="../CoordinadorController/guardarCoordinador" method="post" >
      <input type="hidden" name="idCoordinador" value="<?php echo $coordinador[0]->codigo?>">
      <input type="hidden" name="idPersona" value="<?php echo $coordinador[0]->codigo_persona?>">
      <input type="hidden" name="idDireccion" value="<?php echo $coordinador[0]->codigo_direccion?>">
      <input type="hidden" name="idSocio" value="<?php echo $coordinador[0]->codigo_socio?>">
	  <table class="table table-hover">
		  <h3>Persona</h3>
			<tr>
			  <td width="25%"><!-- text input -->
				  <div class="form-group">
					<label>Nombre</label>
					<input type="text" id="name" name="name" value="<?php echo $coordinador[0]->nombre?>" class="form-control" placeholder="Ingrese el Nombre del Coordinador">
				  </div>
			  </td>
			  <td width="5%"></td>				  
			  <td width="25%">
				<!-- text input -->
				  <div class="form-group">
					<label>Apellido</label>
					<input type="text" id="surname" name="surname" value="<?php echo $coordinador[0]->apellido?>" class="form-control" placeholder="Ingrese el Apellido del Coordinador">
				  </div>
			  </td>	
			  <td width="45%"></td>	
			</tr>
			<tr>
			  <td width="25%">
				  <!-- text input -->
				  <div class="form-group">
					<label>Dni</label>
					<input type="text" id="dni" name="dni" value="<?php echo $coordinador[0]->dni?>" class="form-control" placeholder="Ingrese el Dni del Coordinador">
				  </div>
			  </td>
			  <td width="5%"></td>		
			  <td width="25%"> 
				  <!-- date input -->
				  <div class="form-group">
					<label>Fecha de Nacimiento</label>
					<input type="date" id="birthdate" name="birthdate" value="<?php echo $coordinador[0]->fecha_nacimiento?>" class="form-control" placeholder="Ingrese la Fecha de Nacimiento">
				  </div>
				</td>
			  <td width="45%"></td>	
			</tr>
		  </table>
		  <div class="panel-group">
			  <div class="panel panel-default">
				<div class="panel-heading">
				  <h4 class="panel-title">
					<a data-toggle="collapse" href="#collapse1">Dirección</a>
				  </h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse">					  
					<table class="table table-hover">
						<tr>                  
						  <td width="25%">
							<!-- select input -->
							  <div class="form-group">
								<label>Provincias</label>
								<select id="provincia" name="provincia" class="form-control" onchange="cargarLocalidades();" >							
									<?php
										for ($i = 0; $i < count($provincias); $i++) {
										

									?>
									<option  value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
									
									<?php } ?>
								</select>
							  </div>
						  </td>
						  <td width="5%"></td>
						  <td width="25%"> 
							  <!-- select input -->
							  <div class="form-group">
								<label>Localidades</label>
								<select id="localidad" name="localidad" class="form-control"onchange="cargarBarrios();" >							
									
								</select>
							  </div>
							</td>
						  <td width="45%"></td>
						</tr>
						<tr>                  
						  <td width="25%">
							<!-- select input -->
							  <div class="form-group">
								<label>Barrios</label>
								<select id="barrio" name="barrio" class="form-control"  >							
									
								</select>
							  </div>
						  </td>
						  <td width="5%"></td>
						  <td width="25%"> 
							  <!-- select input -->
							  <div class="form-group">
								<label>Código Postal</label>									
								<input type="text" id="postalCode" name="postalCode" value="<?php echo $direccion[0]->codigo_postal?>" class="form-control" placeholder="Ingrese el Código Postal del Coordinador">
							  </div>
							</td>
						  <td width="45%"></td>
						</tr>
						<tr>
						  <td width="25%"><!-- text input -->
							  <div class="form-group">
								<label>Calle</label>
								<input type="text" id="street" name="street" value="<?php echo $direccion[0]->calle?>"class="form-control" placeholder="Ingrese la calle">
							  </div>
						  </td>
						  <td width="5%"></td>
						  <td width="25%">
							<!-- text input -->
							  <div class="form-group">
								<label>Número</label>
								<input type="number" id="number" name="number" value="<?php echo $direccion[0]->numero?>" class="form-control" placeholder="Ingrese el Numero">
							  </div>
						  </td>
						  <td width="45%"></td>
						</tr>
					</table>	
				</div>
			  </div>
			</div>	
		  <table class="table table-hover">
		  <h3>Socio</h3>
			<tr>
			  <td width="25%"><!-- text input -->
				  <div class="form-group">
					<label>Número de Socio</label>
					<input type="number" id="nroScio" name="nroScio" value="<?php echo $socio[0]->nro_socio?>" class="form-control" placeholder="Ingrese el Número de Socio">
				  </div>
			  </td>
			  <td width="5%"></td>		
			  <td width="25%">
				<!-- text input -->
				  <div class="form-group">
					<label>Cuil</label>
					<input type="number" id="cuil" name="cuil" value="<?php echo $socio[0]->cuil?>" class="form-control" placeholder="Ingrese el Cuil">
				  </div>
			  </td>
			  <td width="45%"></td>		
			</tr>
		  </table>	
			<div class="panel-group">
			  <div class="panel panel-default">
				<div class="panel-heading">
				  <h4 class="panel-title">
					<a data-toggle="collapse" href="#collapseB">Beneficios</a>
				  </h4>
				</div>
				<div id="collapseB" class="panel-collapse collapse">						
			  <a id="agregarCampo" class="btn btn-primary" href="#">Agregar Beneficio</a>
					<div id="contenedor">
						<div class="added">								
							<table class="table table-hover" id="benefitTable_1" name="benefitTable_1">
								<tr>
								  <td width="25%">
									  <!-- text input -->
									  <div class="form-group">
										<label>Beneficio</label>
										<select id="benefit_6" name="benefit_6" class="form-control" onchange="habilitarOtherBenefit(this.id);" >							
											<?php
												for ($i = 0; $i < count($beneficios); $i++) {
												

											?>
											<option  value="<?php echo $beneficios[$i]->codigo?>"><?php echo $beneficios[$i]->nombre?></option>
											<?php } ?>
										</select>
									  </div>
								  </td>
								  <td width="5%"></td>
								  <td width="25%"> 
									  <!-- date input -->
									  <div class="otherBenefitDiv_6" id="otherBenefitDiv_6">
										<label id="otherBenefitLabel_6" name="otherBenefit_6">Otros Beneficios</label>
										<input type="text" id="otherBenefit_6" name="otherBenefit_6" value="<?php echo $beneficioSocio[0]->otros_beneficios?>"class="form-control" placeholder="Ingrese Otro Beneficio que reciba">
									  </div>
									</td>
								  <td width="45%"></td>
								</tr>
								<tr>
									<td width="25%"> 
									  <!-- date input -->
									  <div class="form-group">
										<label>Fecha Inicio</label>
										<input type="date" id="dateStart_6" name="dateStart_6" value="<?php echo $beneficioSocio[0]->fecha_inicio?>" class="form-control" placeholder="Ingrese la Fecha de Inicio">
									  </div>
									</td>
								  <td width="5%"></td>
								  <td width="25%"> 
									  <!-- date input -->
									  <div class="form-group">
										<label>Fecha Fin</label>
										<input type="date" id="dateEnd_6" name="dateEnd_6" value="<?php echo $beneficioSocio[0]->fecha_fin?>" class="form-control" placeholder="Ingrese la Fecha de Fin">
									  </div>
									</td>
								  <td width="45%"></td>
								</tr>
								<tr>                  
								  <td width="25%">
									<!-- number input -->
									  <div class="form-group">
										<label>Observación</label>
										<textarea  id="observation_6" name="observation_6" class="form-control" placeholder="Ingrese Observaciones"><?php echo $beneficioSocio[0]->observacion?></textarea>
									  </div>
								  </td>
								</tr>
							</table>
							<input type="number" id="BenefitIni" name="BenefitIni" style="display:none;">
							<input type="number" id="BenefitFin" name="BenefitFin" style="display:none;">
						</div>						
							<script src="http://code.jquery.com/jquery-latest.js"></script>
							<script type="text/javascript">
								
								//var x = $("#contenedor div").length + 1;
								
								$(document).ready(function() {
							
									var MaxInputs       = 16; //Número Maximo de Campos
									var contenedor       = $("#contenedor"); //ID del contenedor
									var AddButton       = $("#agregarCampo"); //ID del Botón Agregar

									//var x = número de campos existentes en el contenedor
									var x = $("#contenedor div").length + 1;
									FieldCount = x-1; //para el seguimiento de los campos

									$(AddButton).click(function (e) {
										if(x <= MaxInputs) //max input box allowed
										{
											FieldCount++;
											//agregar campo
											
											$(contenedor).append('<div><table name="tabla" class="table table-hover" id="benefitTable_'+ FieldCount +'" ><tr><td width="25%"><div class="form-group"><label>Beneficio</label><select id="benefit_'+ FieldCount +'" name="benefit_'+ FieldCount +'" class="form-control"><?php for ($i = 0; $i < count($beneficios); $i++) {?><option  value="<?php echo $beneficios[$i]->codigo;?>"><?php echo $beneficios[$i]->nombre;?></option><?php } ?></select></div></td><td width="5%"></td><td width="25%"><div class="otherBenefitDiv_'+ FieldCount +'" id="otherBenefitDiv_'+ FieldCount +'"><label id="otherBenefitLabel_'+ FieldCount +'">Otros Beneficios</label><input type="text" id="otherBenefit_'+ FieldCount +'" name="otherBenefit_'+ FieldCount +'" class="form-control" placeholder="Ingrese Otro Beneficio que reciba"></div></td><td width="45%"></td></tr><tr><td width="25%"><div class="form-group"><label>Fecha Inicio</label><input type="date" id="dateStart_'+ FieldCount +'" name="dateStart_'+ FieldCount +'" class="form-control" placeholder="Ingrese la Fecha de Inicio"></div></td><td width="5%"></td><td width="25%"><div class="form-group"><label>Fecha Fin</label><input type="date" id="dateEnd_'+ FieldCount +'" name="dateEnd_'+ FieldCount +'" class="form-control" placeholder="Ingrese la Fecha de Fin"></div></td><td width="45%"></td></tr><tr><td width="25%"><div class="form-group"><label>Observación</label><textarea  id="observation_'+ FieldCount +'" name="observation_'+ FieldCount +'" class="form-control" placeholder="Ingrese Observaciones"></textarea></div></td></tr></table><a href="#" class="eliminar">&times;</a></div>');
											//alert($(contenedor));
											x++; //text box increment
											var benefit       = $("#benefit_"+ FieldCount);
											 benefit.attr('onChange', "habilitarOtherBenefit(this.id);" );	
											$('.otherBenefitDiv_'+ FieldCount).css('display','none');												 
											
											document.getElementById("BenefitFin").value = FieldCount;
										}
										return false;
									});

									$("body").on("click",".eliminar", function(e){ //click en eliminar campo
										if( x > 1 ) {
											$(this).parent('div').remove(); //eliminar el campo
											x--;
										}
										return false;
									});
								});
							</script>
					</div>	
				</div>
			  </div>
			</div>	
        <div class="box-footer">
          <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
        </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateCoordinador.js')?>"></script>
<script>
	document.getElementById("provincia").value=<?php echo $provincia[0]->codigo;?>;
	cargarLocalidades( );
	document.getElementById("localidad").value=<?php echo $localidad[0]->codigo;?>;
	cargarBarrios( );
	document.getElementById("barrio").value=<?php echo $barrio[0]->codigo;?>;
	
	
	
	var len = $("#contenedor div").length;
	var contenedor       = $("#contenedor");
	var codigoInicial = <?php echo "'".$beneficioSocio[0]->codigo."'";?>;
	if( codigoInicial != null){
		document.getElementById("benefit_"+len).value = codigoInicial;
	}
	habilitarOtherBenefit("benefit_"+ len);
	
	var FieldCount = len;;
	document.getElementById("BenefitIni").value = len;
	document.getElementById("BenefitFin").value = FieldCount;
	<?php for($i = 1; $i < count($beneficioSocio); $i++){?>
		$(contenedor).append('<div><table name="tabla" class="table table-hover" id="benefitTable_'+ (len + <?php echo $i;?>) +'" ><tr><td width="25%"><div class="form-group"><label>Beneficio</label><select id="benefit_'+ (len + <?php echo $i;?>) +'" name="benefit_'+ (len + <?php echo $i;?>) +'" class="form-control"><?php for ($j = 0; $j < count($beneficios); $j++) {?><option  value="<?php echo $beneficios[$j]->codigo;?>"><?php echo $beneficios[$j]->nombre;?></option><?php } ?></select></div></td><td width="5%"></td><td width="25%"><div class="otherBenefitDiv_'+ (len + <?php echo $i;?>) +'" id="otherBenefitDiv_'+ (len + <?php echo $i;?>) +'"><label id="otherBenefitLabel_'+ (len + <?php echo $i;?>) +'">Otros Beneficios</label><input type="text" id="otherBenefit_'+ (len + <?php echo $i;?>) +'" name="otherBenefit_'+ (len + <?php echo $i;?>) +'" value="<?php echo $beneficioSocio[$i]->otros_beneficios; ?>" class="form-control" placeholder="Ingrese Otro Beneficio que reciba"></div></td><td width="45%"></td></tr><tr><td width="25%"><div class="form-group"><label>Fecha Inicio</label><input type="date" id="dateStart_'+ (len + <?php echo $i;?>) +'" name="dateStart_'+ (len + <?php echo $i;?>) +'" value="<?php echo $beneficioSocio[$i]->fecha_inicio; ?>" class="form-control" placeholder="Ingrese la Fecha de Inicio"></div></td><td width="5%"></td><td width="25%"><div class="form-group"><label>Fecha Fin</label><input type="date" id="dateEnd_'+ (len + <?php echo $i;?>) +'" name="dateEnd_'+ (len + <?php echo $i;?>) +'" value="<?php echo $beneficioSocio[$i]->fecha_fin; ?>" class="form-control" placeholder="Ingrese la Fecha de Fin"></div></td><td width="45%"></td></tr><tr><td width="25%"><div class="form-group"><label>Observación</label><textarea  id="observation_'+ (len + <?php echo $i;?>) +'" name="observation_'+ (len + <?php echo $i;?>) +'" class="form-control" placeholder="Ingrese Observaciones"><?php echo $beneficioSocio[$i]->observacion; ?></textarea></div></td></tr></table><a href="#" class="eliminar">&times;</a></div>');
		document.getElementById("benefit_"+ (len  + <?php echo $i;?>)).value=<?php echo $beneficioSocio[$i]->codigo;?>;
		var x = len  + <?php echo $i;?>;
		FieldCount = x;		
		document.getElementById("BenefitFin").value = FieldCount;
		var benefit       = $("#benefit_"+ (len  + <?php echo $i;?>));
		benefit.attr('onChange', "habilitarOtherBenefit(this.id);" );	
		//$('.otherBenefitDiv_'+ FieldCount).css('display','none');		
		habilitarOtherBenefit("benefit_"+ (len  + <?php echo $i;?>));
	<?php }?>
	
	
	
	
	
	function cargarLocalidades() {
		var provincia=document.getElementById("provincia").value;
		var localidad=document.getElementById("localidad");
		localidad.options.length =0;
		
		<?php for($i = 0; $i < count($localidades); $i++){?>
			if(<?php echo $localidades[$i]->codigo_provincia; ?> == provincia){				
				var option = document.createElement("option");
				option.value = <?php echo $localidades[$i]->codigo; ?>;
				option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
				localidad.add(option);
			}
			
		<?php }?>
	   cargarBarrios( );
	  }
  
  function cargarBarrios() {
			
	var localidad=document.getElementById("localidad").value;
	var barrio=document.getElementById("barrio");
	barrio.options.length =0;
	
	<?php for($i = 0; $i < count($barrios); $i++){?>
		if(<?php echo $barrios[$i]->codigo_localidad; ?> == localidad){				
			var option = document.createElement("option");
			option.value = <?php echo $barrios[$i]->codigo; ?>;
			option.text = <?php echo "'".$barrios[$i]->nombre."'"; ?>;
			barrio.add(option);
		}
		
	<?php }?>
		   
  }
  
  function habilitarOtherBenefit(id){	
		idField = id.split("_")[1];
		var benefit=document.getElementById("benefit_"+idField).value;
		if(benefit==14){	
			$('.otherBenefitDiv_'+ idField).css('display','block');
		}else{
			$('.otherBenefitDiv_'+ idField).css('display','none');
		}
	}
	
	
</script>


