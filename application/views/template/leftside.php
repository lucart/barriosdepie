<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('public/img/default_user.png')?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $nombre." ".$apellido; ?></p>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <!-- USUARIOS -->
        <?php if($permiso == 1) { ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/UsuarioController/create')?>"><i class="fa fa-circle-o"></i> Nuevo Usuario</a></li>
            <li><a href="<?php echo base_url('index.php/UsuarioController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Usuarios</a></li>
          </ul>
        </li>
        <?php } ?>
        <!-- COMEDOR -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Comedores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/ComedorController/create')?>"><i class="fa fa-circle-o"></i> Nuevo comedor</a></li>
            <li><a href="<?php echo base_url('index.php/ComedorController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Comedores</a></li>
          </ul>
        </li>
        <!-- COORDINADOR -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Coordinadores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/CoordinadorController/create')?>"><i class="fa fa-circle-o"></i> Nuevo coordinador</a></li>
            <li><a href="<?php echo base_url('index.php/CoordinadorController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Coordinadores</a></li>
          </ul>
        </li>
        <!-- MENOR -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Menores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/MenorController/create')?>"><i class="fa fa-circle-o"></i> Nuevo menor</a></li>
            <li><a href="<?php echo base_url('index.php/MenorController/lista')?>"><i class="fa fa-circle-o"></i> Listado de menores</a></li>
          </ul>
        </li>
        <!-- MAYORES -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Mayores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/MayorController/create')?>"><i class="fa fa-circle-o"></i> Nuevo mayor</a></li>
            <li><a href="<?php echo base_url('index.php/MayorController/lista')?>"><i class="fa fa-circle-o"></i> Listado de mayores</a></li>
          </ul>
        </li>
        <!-- REPORTES -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Reportes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/SocioController/socioPorBeneficio')?>"><i class="fa fa-circle-o"></i>Socios por Beneficio</a></li>
          </ul>
        </li>						
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>