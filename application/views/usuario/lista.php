<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Usuarios</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Apellido</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Permiso</th>
                  <th>Acciones</th>
                </tr>
                <?php
                for ($i = 0; $i < count($usuarios); $i++) {    
                ?>
                  <tr>
                    <td><?php echo $usuarios[$i]->codigo;?></td>
                    <td><?php echo $usuarios[$i]->nombre;?></td>
                    <td><?php echo $usuarios[$i]->apellido;?></td>
                    <td><?php echo $usuarios[$i]->email;?></td>
                    <td><?php echo $usuarios[$i]->username;?></td>
                    <td><?php echo $usuarios[$i]->permiso;?></td>
                    <td><button onclick="window.location.href='../UsuarioController/update?idUsuario=<?php echo $usuarios[$i]->codigo;?>'" class="btn btn-warning glyphicon glyphicon-pencil btn-xs"></button>
                        <button onclick="confirmDelete(<?php echo $usuarios[$i]->codigo;?>);" class="btn btn-danger glyphicon glyphicon-trash btn-xs" style="margin-left: 10px;"></button>
                    </td>
                  </tr>
                <?php } ?>
              </table>
            </div>
          </div>
        </div>
</div>
<script>
  function confirmDelete(id) {
    swal({
        title: "Eliminar Usuario",
        text: "¿Está seguro que desea eliminar este usuario?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../UsuarioController/eliminarUsuario?idUsuario=" + id;
        }
    });
  }
</script>