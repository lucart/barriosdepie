<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado De Menores</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Nombre</th>
                  <th>Apellido</th>
                  <th>Dni</th>
                  <th>Fecha Nacimiento</th>
                  <th>Tutor</th>
                  <th>Comedor</th>
                  <th>Acciones</th>
                </tr>
				<?php
					for ($i = 0; $i < count($menores); $i++) {
					

				?>
                <tr>
                  <td><?php echo $menores[$i]->nombre; ?></td>
                  <td><?php echo $menores[$i]->apellido; ?></td>
                  <td><?php echo $menores[$i]->dni;  ?></td>
                  <td><?php echo $menores[$i]->fecha_nacimiento;  ?></td>
                  <td><?php echo $menores[$i]->nombreTutor." ".$menores[$i]->apellidoTutor;  ?></td>
                  <td><?php echo $menores[$i]->comedor;  ?></td>
                  <td><button onclick="window.location.href='../MenorController/update?idMenor=<?php echo $menores[$i]->codigo;?>'" class="btn btn-warning glyphicon glyphicon-pencil btn-xs"></button>
                        <button onclick="confirmDelete(<?php echo $menores[$i]->codigo;?>);" class="btn btn-danger glyphicon glyphicon-trash btn-xs" style="margin-left: 10px;"></button>
                  </td>
                </tr>
				<?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>