<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Editar un Nuevo Menor</h3>
  </div>
  <div class="box-body">
    <form id="guardarMenorForm"  role="form" action="../MenorController/guardarMenor" method="post" > 
        <input type="hidden" name="idMenor" value="<?php echo $menor[0]->codigo?>">
        <input type="hidden" name="idPersona" value="<?php echo $menor[0]->codigo_persona?>">
        <input type="hidden" name="idDireccion" value="<?php echo $menor[0]->codigo_direccion?>">
        <table class="table table-hover">
                <tr>
                  <td width="25%">
              <h3>Tutor</h3>
                  <div class="form-group" style="width: 54%; margin-left: 1%;">
              <br>
              <select id="tutor" name="tutor" class="selectpicker" data-live-search="true" >  
                <option  value="0">Seleccione...</option>   
                <?php
                  for ($i = 0; $i < count($mayores); $i++) {
                    if($mayores[$i]->codigo == $menor[0]->codigo_tutor) {
                ?>
                  <option selected value="<?php echo $mayores[$i]->codigo?>"><?php echo $mayores[$i]->nombre?></option>
                <?php } else {?>
                  <option value="<?php echo $mayores[$i]->codigo?>"><?php echo $mayores[$i]->nombre?></option>
                 <?php } } ?>
              </select>
            </div>    
          </td>
          <td width="5%"></td>
                  <td width="25%">
              <h3>Comedor</h3>
                  <div class="form-group" style="width: 54%; margin-left: 1%;">
              <br>
              <select id="comedor" name="comedor" class="selectpicker" data-live-search="true" >  
                <option  value="0">Seleccione...</option> 
                <?php
                  for ($i = 0; $i < count($comedores); $i++) {
                    if($comedores[$i]->codigo == $menor[0]->codigo_comedor) {
                ?>
                  <option selected value="<?php echo $comedores[$i]->codigo?>"><?php echo $comedores[$i]->nombre?></option>
                <?php } else {?>
                  <option value="<?php echo $comedores[$i]->codigo?>"><?php echo $comedores[$i]->nombre?></option>
                 <?php } } ?>          
              </select>
            </div>  
          </td>
          <td width="45%"></td>
                </tr>
              </table>  
              <table class="table table-hover">
        <h3>Persona</h3>
                <tr>
                  <td width="25%">
            <div class="form-group">
            <label>Nombre</label>
            <input type="text" id="nombre" name="nombre" value="<?php echo $menor[0]->nombre?>" class="form-control" placeholder="Ingrese el Nombre del Menor">
            </div>
          </td>
          <td width="5%"></td>
                  <td width="25%">
            <div class="form-group">
            <label>Apellido</label>
            <input type="text" id="apellido" name="apellido" value="<?php echo $menor[0]->apellido?>" class="form-control" placeholder="Ingrese el Apellido del Menor">
            </div>
          </td>
          <td width="45%"></td>
                </tr>
                <tr>
                  <td width="25%">
            <div class="form-group">
            <label>Dni</label>
            <input type="text" id="dni" name="dni" value="<?php echo $menor[0]->dni?>" class="form-control" placeholder="Ingrese el Dni del Menor">
            </div>
          </td>
                  <td width="5%"></td>
                  <td width="25%"> 
          <div class="form-group">
            <label>Fecha de Nacimiento</label>
            <input type="date" id="fechaNac" name="fechaNac" value="<?php echo $menor[0]->fecha_nacimiento?>" class="form-control" placeholder="Ingrese la Fecha de Nacimiento">
          </div>
          </td>
          <td width="45%"></td>
                </tr>
              </table>  
        <div class="panel-group" style="width: 55%;">
          <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapse1">Dirección</a>
            </h4>
          </div>
          <div id="collapse1" class="panel-collapse collapse">            
            <table class="table table-hover">
              <tr>                  
                <td width="50%" style="padding-left: 5%">
                <!-- select input -->
                  <div class="form-group">
                  <label>Provincia</label>
                  <select id="provincia" name="provincia" class="form-control" onchange="cargarLocalidades();" >  
                    <option  value="0">Seleccione...</option>    
                    <?php
                      for ($i = 0; $i < count($provincias); $i++) {
                        if($provincias[$i]->codigo == $menor[0]->codigo_provincia) {
                    ?>
                      <option selected value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
                    <?php } else {?>
                      <option value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
                     <?php } } ?>  
                  </select>
                  </div>
                </td>
                <td width="5%"></td>
                <td width="45%"> 
                  <!-- select input -->
                  <div class="form-group">
                  <label>Localidad</label>
                  <select id="localidad" name="localidad" class="form-control"onchange="cargarBarrios();" >             
                    
                  </select>
                  </div>
                </td>
                <td width="45%"></td>
              </tr>
              <tr>                  
                <td width="25%" style="padding-left: 5%">
                <!-- select input -->
                  <div class="form-group">
                  <label>Barrio</label>
                  <select id="barrio" name="barrio" class="form-control"  >             
                    
                  </select>
                  </div>
                </td>
                <td width="5%"></td>
                <td width="25%"> 
                  <!-- select input -->
                  <div class="form-group">
                  <label>Código Postal</label>                  
                  <input type="text" id="codigoPostal" name="codigoPostal" value="<?php echo $menor[0]->codigo_postal?>" class="form-control" placeholder="Ingrese el Código Postal del Menor">
                  </div>
                </td>
                <td width="45%"></td>
              </tr>
              <tr>
                <td width="25%" style="padding-left: 5%">
                  <div class="form-group">
                  <label>Calle</label>
                  <input type="text" id="calle" name="calle" value="<?php echo $menor[0]->calle?>" class="form-control" placeholder="Ingrese la calle">
                  </div>
                </td>
                <td width="5%"></td>
                <td width="25%">
                <!-- text input -->
                  <div class="form-group">
                  <label>Número</label>
                  <input type="number" id="numero" name="numero" value="<?php echo $menor[0]->numero?>" class="form-control" placeholder="Ingrese el Numero">
                  </div>
                </td>
                <td width="45%"></td>
              </tr>
            </table>  
          </div>
          </div>
        </div>  
        <div class="box-footer">
          <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
        </div>
    </form>
  </div>
</div>
<script>
  function cargarLocalidades(){
      var provincia=document.getElementById("provincia").value;
      var localidad=document.getElementById("localidad");
      localidad.options.length =0;
      
      var optionSeleccione = document.createElement("option");
          optionSeleccione.value = 0;
          optionSeleccione.text = "Seleccione...";
          localidad.add(optionSeleccione);

      <?php for($i = 0; $i < count($localidades); $i++){?>
        if(<?php echo $localidades[$i]->codigo_provincia; ?> == provincia){       
          var option = document.createElement("option");
          option.value = <?php echo $localidades[$i]->codigo; ?>;
          option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
          localidad.add(option);
        }
        
      <?php }?>
      localidad.value = <?php echo $menor[0]->codigo_localidad; ?>;
       cargarBarrios();
  }

  function cargarBarrios(){
      
      var localidad=document.getElementById("localidad").value;
      var barrio=document.getElementById("barrio");
      barrio.options.length =0;
      
      var optionSeleccione = document.createElement("option");
          optionSeleccione.value = 0;
          optionSeleccione.text = "Seleccione...";
          barrio.add(optionSeleccione);

      <?php for($i = 0; $i < count($barrios); $i++){?>
        if(<?php echo $barrios[$i]->codigo_localidad; ?> == localidad){       
          var option = document.createElement("option");
          option.value = <?php echo $barrios[$i]->codigo; ?>;
          option.text = <?php echo "'".$barrios[$i]->nombre."'"; ?>;
          barrio.add(option);
        }
      barrio.value = <?php echo $menor[0]->codigo_barrio; ?>;
      <?php }?>
       
  }
</script>
<script type="text/javascript">
function cargarCombos(){
  cargarLocalidades();
  cargarBarrios();
}

window.onload = cargarCombos();


</script>
<script src="<?php echo base_url('public/js/validateMenor.js')?>"></script>

