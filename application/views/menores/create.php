<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Crear un Nuevo Menor</h3>
  </div>
  <div class="box-body">
    <form id="guardarMenorForm"  role="form" action="../MenorController/guardarMenor" method="post" >	
    		<table class="table table-hover">
                <tr>
                  <td width="25%">
					  	<h3>Tutor</h3>
			            <div class="form-group" style="width: 54%; margin-left: 1%;">
							<br>
							<select id="tutor" name="tutor" class="selectpicker" data-live-search="true" >	
								<option  value="0">Seleccione...</option>						
								<?php
									for ($i = 0; $i < count($mayores); $i++) {
								?>
								<option  value="<?php echo $mayores[$i]->codigo?>"><?php echo $mayores[$i]->nombre." ".$mayores[$i]->apellido." ".$mayores[$i]->dni ?></option>
								<?php } ?>
							</select>
						</div>		
				  </td>
				  <td width="5%"></td>
                  <td width="25%">
					  	<h3>Comedor</h3>
			            <div class="form-group" style="width: 54%; margin-left: 1%;">
							<br>
							<select id="comedor" name="comedor" class="selectpicker" data-live-search="true" >	
								<option  value="0">Seleccione...</option>						
								<?php
									for ($i = 0; $i < count($comedores); $i++) {
								?>
								<option  value="<?php echo $comedores[$i]->codigo?>"><?php echo $comedores[$i]->nombre?></option>
								<?php } ?>
							</select>
						</div>	
				  </td>
				  <td width="45%"></td>
                </tr>
              </table>	
              <table class="table table-hover">
			  <h3>Persona</h3>
                <tr>
                  <td width="25%">
					  <div class="form-group">
						<label>Nombre</label>
						<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ingrese el Nombre del Menor">
					  </div>
				  </td>
				  <td width="5%"></td>
                  <td width="25%">
					  <div class="form-group">
						<label>Apellido</label>
						<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Ingrese el Apellido del Menor">
					  </div>
				  </td>
				  <td width="45%"></td>
                </tr>
                <tr>
                  <td width="25%">
					  <div class="form-group">
						<label>Dni</label>
						<input type="text" id="dni" name="dni" class="form-control" placeholder="Ingrese el Dni del Menor">
					  </div>
				  </td>
                  <td width="5%"></td>
                  <td width="25%"> 
					<div class="form-group">
						<label>Fecha de Nacimiento</label>
						<input type="date" id="fechaNac" name="fechaNac" class="form-control" placeholder="Ingrese la Fecha de Nacimiento">
					</div>
				  </td>
				  <td width="45%"></td>
                </tr>
              </table>	
				<div class="panel-group" style="width: 55%;">
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse1">Dirección</a>
					  </h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse">					  
						<table class="table table-hover">
							<tr>                  
							  <td width="50%" style="padding-left: 5%">
								<!-- select input -->
								  <div class="form-group">
									<label>Provincias</label>
									<select id="provincia" name="provincia" class="form-control" onchange="cargarLocalidades();" >	
										<option  value="0">Seleccione...</option>						
										<?php
											for ($i = 0; $i < count($provincias); $i++) {
											

										?>
										<option  value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
										
										<?php } ?>
									</select>
								  </div>
							  </td>
							  <td width="5%"></td>
							  <td width="45%"> 
								  <!-- select input -->
								  <div class="form-group">
									<label>Localidades</label>
									<select id="localidad" name="localidad" class="form-control"onchange="cargarBarrios();" >							
										
									</select>
								  </div>
								</td>
								<td width="45%"></td>
							</tr>
							<tr>                  
							  <td width="25%" style="padding-left: 5%">
								<!-- select input -->
								  <div class="form-group">
									<label>Barrios</label>
									<select id="barrio" name="barrio" class="form-control"  >							
										
									</select>
								  </div>
							  </td>
							  <td width="5%"></td>
							  <td width="25%"> 
								  <!-- select input -->
								  <div class="form-group">
									<label>Código Postal</label>									
									<input type="text" id="codigoPostal" name="codigoPostal" class="form-control" placeholder="Ingrese el Código Postal del Menor">
								  </div>
								</td>
							  <td width="45%"></td>
							</tr>
							<tr>
							  <td width="25%" style="padding-left: 5%"><!-- text input -->
								  <div class="form-group">
									<label>Calle</label>
									<input type="text" id="calle" name="calle" class="form-control" placeholder="Ingrese la calle">
								  </div>
							  </td>
							  <td width="5%"></td>
							  <td width="25%">
								<!-- text input -->
								  <div class="form-group">
									<label>Número</label>
									<input type="number" id="numero" name="numero" class="form-control" placeholder="Ingrese el Numero">
								  </div>
							  </td>
							  <td width="45%"></td>
							</tr>
						</table>	
					</div>
				  </div>
				</div>	
	      <div class="box-footer">
	        <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
	      </div>
    </form>
  </div>
</div>
<script>
cargarLocalidades( );
cargarBarrios( );
	
	
	function cargarLocalidades( ){
			var provincia=document.getElementById("provincia").value;
			var localidad=document.getElementById("localidad");
			localidad.options.length =0;
			
			var optionSeleccione = document.createElement("option");
					optionSeleccione.value = 0;
					optionSeleccione.text = "Seleccione...";
					localidad.add(optionSeleccione);

			<?php for($i = 0; $i < count($localidades); $i++){?>
				if(<?php echo $localidades[$i]->codigo_provincia; ?> == provincia){				
					var option = document.createElement("option");
					option.value = <?php echo $localidades[$i]->codigo; ?>;
					option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
					localidad.add(option);
				}
				
			<?php }?>
		   cargarBarrios( );
	}

	function cargarBarrios( ){
			
			var localidad=document.getElementById("localidad").value;
			var barrio=document.getElementById("barrio");
			barrio.options.length =0;
			
			var optionSeleccione = document.createElement("option");
					optionSeleccione.value = 0;
					optionSeleccione.text = "Seleccione...";
					barrio.add(optionSeleccione);

			<?php for($i = 0; $i < count($barrios); $i++){?>
				if(<?php echo $barrios[$i]->codigo_localidad; ?> == localidad){				
					var option = document.createElement("option");
					option.value = <?php echo $barrios[$i]->codigo; ?>;
					option.text = <?php echo "'".$barrios[$i]->nombre."'"; ?>;
					barrio.add(option);
				}
				
			<?php }?>
		   
	}
</script>
<script src="<?php echo base_url('public/js/validateMenor.js')?>"></script>

